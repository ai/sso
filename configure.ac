AC_INIT([sso], [2.0], [info@autistici.org])
AC_CONFIG_SRCDIR([src/sso/sso.c])
AC_LANG(C++)

AM_INIT_AUTOMAKE([dist-bzip2 foreign])
AC_CONFIG_HEADERS(src/sso/config.h)
AC_CONFIG_MACRO_DIR([m4])
dnl AC_DISABLE_SHARED

dnl Program checks.
AC_PROG_CC
AC_PROG_CXX
AC_PROG_INSTALL
AC_PROG_LN_S
AC_PROG_MAKE_SET
AC_PROG_LIBTOOL
AC_SUBST(LIBTOOL_DEPS)

CFLAGS="$CFLAGS -fPIC -std=c99 -pedantic"
CXXFLAGS="$CXXFLAGS -fPIC"

dnl Checks for libraries.
ACX_PTHREAD([have_pthread=yes], [have_pthread=no])
CXXFLAGS="$CXXFLAGS $PTHREAD_CFLAGS"
LIBS="$PTHREAD_LIBS $LIBS"

dnl Checks for headers.
AC_HEADER_STDC
AC_HEADER_TIME

dnl Apache2 SSO module.
AC_ARG_ENABLE(mod-sso,
  AS_HELP_STRING([--enable-mod-sso],
                 [build the Apache2 mod_sso module]),
  [build_mod_sso=$enableval],
  [build_mod_sso=no])

dnl Checks for apxs.
if test "$build_mod_sso" != "no" ; then
  AX_WITH_APXS()
  APACHE_CFLAGS="-I`${APXS} -q INCLUDEDIR`"
  AC_SUBST(APACHE_CFLAGS)
  APACHE_LIBEXEC_DIR="`${APXS} -q LIBEXECDIR`"
  AC_SUBST(APACHE_LIBEXEC_DIR)

  PKG_CHECK_MODULES(APR, [apr-1, apr-util-1])
  AC_SUBST(APR_CFLAGS)
  AC_SUBST(APR_LIBS)
fi
AM_CONDITIONAL(ENABLE_MOD_SSO, [ test "$build_mod_sso" != "no" ])

dnl PAM SSO module.
AC_ARG_ENABLE(pam-sso,
  AS_HELP_STRING([--enable-pam-sso],
                 [build the PAM SSO module]),
  [build_pam_sso=$enableval],
  [build_pam_sso=no])

dnl Probe for the functionality of the PAM libraries and their include file
dnl naming.  Mac OS X puts them in pam/* instead of security/*.
if test "$build_pam_sso" != "no" ; then
  dnl Do not add -lpam to LIBS, set PAM_LIBS instead.
  save_LIBS="$LIBS"
  AC_SEARCH_LIBS([pam_set_data], [pam], [], [
    AC_MSG_ERROR([libpam not found])])
  LIBS="$save_LIBS"
  AC_SUBST(PAM_LIBS, [-lpam])
  AC_CHECK_FUNCS([pam_getenv pam_getenvlist pam_modutil_getpwnam])
  AC_REPLACE_FUNCS([pam_syslog pam_vsyslog])
  AC_CHECK_HEADERS([security/pam_modutil.h], [],
      [AC_CHECK_HEADERS([pam/pam_modutil.h])])
  AC_CHECK_HEADERS([security/pam_appl.h], [],
      [AC_CHECK_HEADERS([pam/pam_appl.h], [],
          [AC_MSG_ERROR([No PAM header files found])])])
  AC_CHECK_HEADERS([security/pam_ext.h], [],
      [AC_CHECK_HEADERS([pam/pam_ext.h])])
  RRA_HEADER_PAM_CONST

  AC_SUBST(PAMDIR, "\$(exec_prefix)/lib/security")
  AC_ARG_WITH(pam-dir,
    AC_HELP_STRING([--with-pam-dir=DIR],
                   [Where to install PAM module [[PREFIX/lib/security]]]),
              [case "${withval}" in
              /*) PAMDIR="${withval}";;
              ./*|../*) AC_MSG_ERROR(Bad value for --with-pam-dir);;
              *)  PAMDIR="\$(exec_prefix)/lib/${withval}";;
              esac])
  AC_MSG_NOTICE([PAM installation path $PAMDIR])
fi
AM_CONDITIONAL(ENABLE_PAM_SSO, [ test "$build_pam_sso" != "no" ])

dnl Python-dev (actually only used for $PYTHON)
AX_PYTHON_DEVEL

dnl nosetests
AC_PATH_PROG([NOSETESTS], [nosetests${PYTHON_VERSION}])

dnl GoogleTest (use the embedded version)
GTEST_LIBS="\$(top_builddir)/lib/gtest/libgtest.la"
GTEST_CPPFLAGS="-I\$(top_srcdir)/lib/gtest/include"
AC_SUBST(GTEST_LIBS)
AC_SUBST(GTEST_LDFLAGS)
AC_SUBST(GTEST_CPPFLAGS)

dnl LCOV/GCOV support
AC_TDD_GCOV
AC_SUBST(COVERAGE_CFLAGS)
AC_SUBST(COVERAGE_CXXFLAGS)
AC_SUBST(COVERAGE_LDFLAGS)
if test "x$use_gcov" = "xyes"; then
   CFLAGS="$CFLAGS $COVERAGE_CFLAGS"
   CXXFLAGS="$CXXFLAGS $COVERAGE_CXXFLAGS"
   LDFLAGS="$LDFLAGS $COVERAGE_LDFLAGS"
fi

dnl Final stage
AC_OUTPUT(
Makefile
lib/Makefile
lib/gtest/Makefile
src/Makefile
src/sso/Makefile
src/sso/test/Makefile
src/python/Makefile
src/sso_server/Makefile
src/mod_sso/Makefile
src/mod_sso/test/Makefile
src/pam_sso/Makefile
src/sso/Doxyfile)
