#!/bin/sh

# Install our Debian repository.
echo 'deb http://deb.autistici.org/urepo jessie-ai/' \
  > /etc/apt/sources.list.d/ai.list
wget -nv -O- http://deb.autistici.org/repo.key | apt-key add -
apt-get -qq update

PKG="ai-sso ai-sso-server apache2-mpm-worker libapache2-mod-sso python-gevent"
DEBIAN_FRONTEND=noninteractive apt-get install -q -y $PKG

# Edit the hosts file.
ip=$(hostname -I)
cat >> /etc/hosts <<EOF
${ip} login.sso.net www.sso.net app.sso.net
EOF

# Generate keys.
ssotool --gen-keys --public-key /etc/sso/public.key --secret-key /etc/sso/secret.key
chmod 0400 /etc/sso/secret.key
chown ai-sso /etc/sso/secret.key

# Setup the SSO server.
cp /vagrant/sso_config /etc/sso/config
cat >/etc/default/ai-sso-server <<EOF
NO_START=0
SSO_PORT=5002
EOF
service ai-sso-server start

# Setup Apache and create website content.
cp /vagrant/apache_config /etc/apache2/sites-available/000-default.conf
a2enmod rewrite proxy_http ssl sso
mkdir -p /var/www/default /var/www/app
echo '<h1>This is the default website</h1>' > /var/www/default/index.html
echo '<h1>This is the protected app</h1>' > /var/www/app/index.html
cat >/etc/apache2/ports.conf <<EOF
Listen 80
Listen 443
EOF
apache2ctl restart

