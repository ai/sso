#!/bin/bash

set -e
set -u

coverage_dir=./coverage-$$
mkdir $coverage_dir

src_dir=$(pwd)

# upload to public directory if we're running on code.a.o
if [ "$(hostname -s)" = "code" ]; then
  coverage_output=/var/www/ai-sso/coverage
  coverage_url="http://code.autistici.org/project/ai-sso/htdocs/coverage/"
else
  coverage_output=$src_dir/coverage
  coverage_url="file://$coverage_output"
fi
test -d $coverage_output || mkdir $coverage_output
lcov --directory $coverage_dir --zerocounters

coverage_flags="-fprofile-arcs -ftest-coverage"
(cd $coverage_dir ;
 $src_dir/configure CXXFLAGS="$coverage_flags" CFLAGS="$coverage_flags" &&
 make -k check)

lcov -k $src_dir --directory $coverage_dir --capture --output-file $coverage_output/cov.dat
# remove the system headers from the results
lcov -r $coverage_output/cov.dat '/usr/*' --output-file $coverage_output/cov.dat
(cd $coverage_output ; genhtml cov.dat)

rm -fr $coverage_dir

cat <<EOF


*** Test coverage computation complete.
***
*** Results available on:
*** $coverage_url

EOF
