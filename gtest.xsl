<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" indent="yes"/> 
 <xsl:template match="/">
    <unit_tests>
       <xsl:apply-templates/>
    </unit_tests>
 </xsl:template>
 <xsl:template match="testsuite/testsuite/testcase">
     <test>
        <status>
	  <xsl:if test="@status = 'run'">success</xsl:if>
	  <xsl:if test="@status != 'run'">failure</xsl:if>
	</status>
        <fixture>
	  <xsl:value-of select="@classname" />
        </fixture>
        <type>test</type>
        <name>
	  <xsl:value-of select="@name" />
        </name>
     </test>
  </xsl:template>
</xsl:stylesheet>
