/* Copyright (c) 2012 Autistici/Inventati <info@autistici.org>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include "pam_sso.h"

static const char *kPasswordPrompt = "Password: ";

static int read_from_file(const char *path, unsigned char *out, size_t sz) {
  int fd;
  size_t n;
  fd = open(path, O_RDONLY);
  if (fd < 0) {
    return -1;
  }
  n = read(fd, out, sz);
  close(fd);
  return (n == sz) ? 0 : -1;
}

static void free_config(struct pam_sso_config *cfg) {
  if (cfg->public_key != NULL) {
    free(cfg->public_key);
  }
  if (cfg->required_groups != NULL) {
    free(cfg->required_groups);
  }
}

static int parse_config(pam_handle_t *pamh, struct pam_sso_config *cfg, int argc, const char **argv) {
  for (const char **argp = argv; argc--; argp++) {
    const char *arg = *argp;
    if (!strcmp(arg, "debug")) {
      cfg->debug = 1;
    } else if (!strcmp(arg, "use_first_pass")) {
      cfg->use_first_pass = 1;
    } else if (!strcmp(arg, "quiet_fail")) {
      cfg->quiet_fail = 1;
    } else if (!strncmp(arg, "login_server=", 12)) {
      cfg->login_server = arg + 12;
    } else if (!strncmp(arg, "domain=", 7)) {
      cfg->domain = arg + 7;
    } else if (!strncmp(arg, "service=", 8)) {
      cfg->service = arg + 8;
    } else if (!strncmp(arg, "group=", 6)) {
      if (cfg->required_groups == NULL) {
        cfg->required_groups = (const char **)malloc(sizeof(char*)*2);
      } else {
        cfg->required_groups = (const char **)realloc(cfg->required_groups, sizeof(char*)*(cfg->n_required_groups + 2));
      }
      if (cfg->required_groups == NULL) {
        pam_syslog(pamh, LOG_ERR, "out of memory");
        return -1;
      }
      cfg->required_groups[cfg->n_required_groups] = arg + 6;
      cfg->required_groups[cfg->n_required_groups + 1] = NULL;
      cfg->n_required_groups++;
    } else if (!strncmp(arg, "key=", 4)) {
      unsigned char *pk = (unsigned char *)malloc(SSO_PUBLIC_KEY_SIZE);
      if (pk == NULL) {
        pam_syslog(pamh, LOG_ERR, "out of memory");
        return -1;
      }        
      if (read_from_file(arg + 4, pk, SSO_PUBLIC_KEY_SIZE) < 0) {
        pam_syslog(pamh, LOG_ERR, "error loading public key file: %s", strerror(errno));
        return -1;
      }
      cfg->public_key = pk;
    }
  }
  return 0;
}

static int authenticate(pam_handle_t *pamh,
                        struct pam_sso_config *cfg,
                        const char *username,
                        const char *ticket_string)
{
  sso_ticket_t t;
  int r, retval = 0;

  if (cfg->public_key == NULL) {
    pam_syslog(pamh, LOG_INFO, "no public key set");
    return 0;
  }

  r = sso_ticket_open(&t, ticket_string, cfg->public_key);
  if (r != SSO_OK) {
    if (cfg->debug) {
      pam_syslog(pamh, LOG_INFO, "error decoding ticket: %s", sso_strerror(r));
    }
    return 0;
  }

  // Validate the ticket, and perform an additional check that the
  // username matches.
  r = sso_validate(t, cfg->service, cfg->domain, NULL, cfg->required_groups);
  if (r == SSO_OK) {
    if (!strcmp(t->user, username)) {
      retval = 1;
    } else {
      if (cfg->debug) {
        pam_syslog(pamh, LOG_INFO, "user in ticket does not match request (%s vs %s)", t->user, username);
      }
    }
  } else {
    if (cfg->debug) {
      pam_syslog(pamh, LOG_INFO, "error validating ticket: %s", sso_strerror(r));
    }
  }

  sso_ticket_free(t);
  return retval;
}

int pam_sm_authenticate(pam_handle_t *pamh, int flags, int argc, const char **argv)
{
  struct pam_sso_config cfg = {0};
  const char *user = NULL, *password = NULL;

  // Parse configuration.
  if (parse_config(pamh, &cfg, argc, argv) < 0) {
    return PAM_SERVICE_ERR;
  }

  // Retrieve user from PAM.
  if (pam_get_user(pamh, &user, NULL) != PAM_SUCCESS) {
    pam_syslog(pamh, LOG_ERR, "could not retrieve user");
    goto error;
  }

  // Retrieve password
  if (cfg.use_first_pass) {
    if (pam_get_item(pamh, PAM_AUTHTOK, (const void **)&password)
        != PAM_SUCCESS) {
      pam_syslog(pamh, LOG_ERR, "could not retrieve password");
      goto error;
    }
  } else {
    // Ask for the password interactively.
    struct pam_conv *conv;
    struct pam_message *pmsg[1], msg[1];
    struct pam_response *resp;

    if (pam_get_item(pamh, PAM_CONV, (const void **)&conv) != PAM_SUCCESS) {
      goto error;
    }
    pmsg[0] = &msg[0];
    msg[0].msg = (char *)kPasswordPrompt;
    msg[0].msg_style = PAM_PROMPT_ECHO_OFF;
    if (conv->conv(1, (const struct pam_message **)pmsg,
                   &resp, conv->appdata_ptr) != PAM_SUCCESS) {
      goto error;
    }
    password = resp->resp;
  }

  // Authenticate.
  if (!authenticate(pamh, &cfg, user, password)) {
    if (!cfg.quiet_fail) {
      pam_syslog(pamh, LOG_ERR, "authentication failed");
    }
    goto error;
  }

  free_config(&cfg);
  return PAM_SUCCESS;

 error:
  free_config(&cfg);
  return PAM_AUTH_ERR;
}

int pam_sm_setcred(pam_handle_t *pamh, int flags, int argc, const char **argv)
{
  return PAM_SUCCESS;
}
