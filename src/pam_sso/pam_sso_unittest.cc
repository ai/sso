
#include <stdarg.h>
#include <stdlib.h>
#include <unistd.h>
#include <gtest/gtest.h>
#include <string>
#include <fstream>
#include <iostream>

#include "sso.h"

extern "C" {
#include "pam_sso.h"
}

/*
 * EXTRA UGLY HACK!!!!1!1
 *
 * We include the pam_sso.c code in this file, but we redefine the
 * libpam calls with our own, so that we can get around the problem of
 * the opaque pam_handle_t.
 */

struct test_pam_handle {
  const char *user;
  char *password;
};
typedef struct test_pam_handle test_pam_handle_t;

extern "C" {

int test_pam_get_item(test_pam_handle_t *pamh, int what, const void **ptr) {
  switch (what) {
  case PAM_CONV:
    *ptr = NULL;                  // No pam_conv support.
    break;

  case PAM_AUTHTOK:
    *ptr = (const void *)pamh->password;
    break;

  default:
    return -1;
  }
  return PAM_SUCCESS;
}

int test_pam_get_user(test_pam_handle_t *pamh, const char **user, void *unused) {
  *user = pamh->user;
  return PAM_SUCCESS;
}

void test_pam_syslog(test_pam_handle_t *pamh, int priority, const char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap);
  va_end(ap);
  fprintf(stderr, "\n");
}

#define pam_handle_t test_pam_handle_t
#define pam_get_user test_pam_get_user
#define pam_get_item test_pam_get_item
#define pam_sm_authenticate test_pam_sm_authenticate
#define pam_sm_setcred test_pam_sm_setcred
#define pam_syslog test_pam_syslog
  
#include "pam_sso.c"

}

using std::string;

namespace {

class PAMTest : public testing::Test {
protected:

  virtual void SetUp() {
    sso_generate_keys(public_key, secret_key);

    // Create a temporary file to store the key.
    char temp_file[] = "/tmp/publickey.XXXXXX";
    int fd = mkstemp(temp_file);
    ASSERT_GE(write(fd, (const char *)public_key, SSO_PUBLIC_KEY_SIZE), 0);
    close(fd);
    public_key_file = temp_file;
  }

  virtual void TearDown() {
    unlink(public_key_file.c_str());
  }
  
  // Return a signed ticket, for test data generation.
  char *sign_ticket(sso_ticket_t t) {
    char buf[1024];
    EXPECT_EQ(0, sso_ticket_sign(t, secret_key, buf, sizeof(buf)));
    // No further use for the original ticket.
    sso_ticket_free(t);
    return strdup(buf);
  }

  string public_key_file;
  unsigned char public_key[SSO_PUBLIC_KEY_SIZE];
  unsigned char secret_key[SSO_SECRET_KEY_SIZE];
};

TEST_F(PAMTest, AuthOk) {
  char *tkt_str = sign_ticket(sso_ticket_new("user", "service/", "domain", NULL, NULL, 7200));
  test_pam_handle_t pamh = {"user", tkt_str};
  string key_arg(string("key=") + public_key_file);
  const char *pam_argv[] = {"service=service/", "domain=domain", "use_first_pass", key_arg.c_str(), NULL};

  ASSERT_EQ(pam_sm_authenticate(&pamh, 0, 4, pam_argv), PAM_SUCCESS)
    << "pam_sm_authenticate failed";
}

TEST_F(PAMTest, AuthFailIfNoKey) {
  char *tkt_str = sign_ticket(sso_ticket_new("user", "service/", "domain", NULL, NULL, 7200));
  test_pam_handle_t pamh = {"user", tkt_str};
  const char *pam_argv[] = {"service=another-service/", "domain=domain", "use_first_pass", NULL};

  ASSERT_EQ(pam_sm_authenticate(&pamh, 0, 3, pam_argv), PAM_AUTH_ERR)
    << "pam_sm_authenticate succeeded unexpectedly";
}

TEST_F(PAMTest, AuthFailOnBadService) {
  char *tkt_str = sign_ticket(sso_ticket_new("user", "service/", "domain", NULL, NULL, 7200));
  test_pam_handle_t pamh = {"user", tkt_str};
  string key_arg(string("key=") + public_key_file);
  const char *pam_argv[] = {"service=another-service/", "domain=domain", "use_first_pass", key_arg.c_str(), NULL};

  ASSERT_EQ(pam_sm_authenticate(&pamh, 0, 4, pam_argv), PAM_AUTH_ERR)
    << "pam_sm_authenticate succeeded unexpectedly";
}

TEST_F(PAMTest, AuthFailIfDifferentUser) {
  char *tkt_str = sign_ticket(sso_ticket_new("user", "service/", "domain", NULL, NULL, 7200));
  test_pam_handle_t pamh = {"another_user", tkt_str};
  string key_arg(string("key=") + public_key_file);
  const char *pam_argv[] = {"service=service/", "domain=domain", "use_first_pass", key_arg.c_str(), NULL};

  ASSERT_EQ(pam_sm_authenticate(&pamh, 0, 4, pam_argv), PAM_AUTH_ERR)
    << "pam_sm_authenticate succeeded unexpectedly";
}

}  // namespace

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
