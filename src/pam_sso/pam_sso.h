// -*- c++ -*-
/* Copyright (c) 2012 Autistici/Inventati <info@autistici.org>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef __sso_pam_sso_H
#define __sso_pam_sso_H 1

#include "config.h"

/* Linux PAM 1.1.0 requires sys/types.h before security/pam_modutil.h. */
#include <sys/types.h>

/* These variables need to be defined. */
#define PAM_SM_AUTH

#ifndef HAVE_PAM_MODUTIL_GETPWNAM
# include <pwd.h>
#endif
#if defined(HAVE_SECURITY_PAM_APPL_H)
# include <security/pam_appl.h>
# include <security/pam_modules.h>
#elif defined(HAVE_PAM_PAM_APPL_H)
# include <pam/pam_appl.h>
# include <pam/pam_modules.h>
#endif
#if defined(HAVE_SECURITY_PAM_EXT_H)
# include <security/pam_ext.h>
#elif defined(HAVE_PAM_PAM_EXT_H)
# include <pam/pam_ext.h>
#endif
#if defined(HAVE_SECURITY_PAM_MODUTIL_H)
# include <security/pam_modutil.h>
#elif defined(HAVE_PAM_PAM_MODUTIL_H)
# include <pam/pam_modutil.h>
#endif
#include <stdarg.h>

#include <sso/sso.h>

struct pam_sso_config {
  const char *login_server;
  const char *domain;
  const char *service;
  unsigned char *public_key;
  const char **required_groups;
  int n_required_groups;
  int debug;
  int use_first_pass;
  int quiet_fail;
};

/**
int pam_sm_authenticate(pam_handle_t *pamh, int flags,
                        int argc, const char **argv);
int pam_sm_setcred(pam_handle_t *pamh, int flags,
                   int argc, const char **argv);
**/

#endif
