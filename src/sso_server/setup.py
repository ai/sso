import os

from setuptools import setup, find_packages

def _find_static_data(module, base_path):
    result = []
    abs_base = os.path.join(module, base_path)
    for root, dirs, files in os.walk(abs_base):
        for f in files:
            result.append(os.path.join(root[len(module)+1:], f))
    return result

setup(name='sso_server',
      version='0.3',
      description='A/I SSO Server',
      install_requires=['flup'],
      zip_safe=False,
      packages=find_packages(),
      package_data={'sso_server':(_find_static_data('sso_server', 'templates')
                                  + _find_static_data('sso_server', 'saml/templates')
                                  + _find_static_data('sso_server', 'static'))},
      entry_points={
        'console_scripts': [
          'sso_server = sso_server.main:main',
        ],
      },
      )
