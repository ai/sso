# Entry point for gunicorn and mod_wsgi.

import os
from sso_server import application

app = application.create_app(
    os.getenv('APP_CONFIG', '/etc/sso/config'))
