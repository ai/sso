# -*- coding: utf-8 -*-
"""
Signing code goes here.
"""
from __future__ import absolute_import
import hashlib
import logging
import string
import M2Crypto

from .app import saml_app
from .codex import nice64
from .xml_templates import SIGNED_INFO, SIGNATURE

logger = logging.getLogger('saml')


def load_certificate(filename):
    logger.info('Using certificate file: {}'.format(filename))
    certificate = M2Crypto.X509.load_cert(filename)
    return ''.join(certificate.as_pem().split('\n')[1:-2])


def load_private_key(filename):
    logger.info('Using private key file: {}'.format(filename))

    # The filename need to be encoded because it is using a C extension under
    # the hood which means it expects a 'const char*' type and will fail with
    # unencoded unicode string.
    return M2Crypto.EVP.load_key(filename.encode('utf-8'))


def sign_with_rsa(private_key, data):
    private_key.sign_init()
    private_key.sign_update(data)
    return nice64(private_key.sign_final())


def get_signature_xml(subject, reference_uri):
    """
    Returns XML Signature for subject.
    """
    logger.debug('get_signature_xml - Begin.')
    private_key = saml_app.saml_private_key
    certificate = saml_app.saml_certificate

    logger.debug('Subject: ' + subject)

    # Hash the subject.
    subject_hash = hashlib.sha1()
    subject_hash.update(subject)
    subject_digest = nice64(subject_hash.digest())
    logger.debug('Subject digest: ' + subject_digest)

    # Create signed_info.
    signed_info = string.Template(SIGNED_INFO).substitute({
        'REFERENCE_URI': reference_uri,
        'SUBJECT_DIGEST': subject_digest,
        })
    logger.debug('SignedInfo XML: ' + signed_info)

    rsa_signature = sign_with_rsa(private_key, signed_info)
    logger.debug('RSA Signature: ' + rsa_signature)

    # Put the signed_info and rsa_signature into the XML signature.
    signed_info_short = signed_info.replace(' xmlns:ds="http://www.w3.org/2000/09/xmldsig#"', '')
    signature_xml = string.Template(SIGNATURE).substitute({
        'RSA_SIGNATURE': rsa_signature,
        'SIGNED_INFO': signed_info_short,
        'CERTIFICATE': certificate,
        })
    logger.info('Signature XML: ' + signature_xml)
    return signature_xml
