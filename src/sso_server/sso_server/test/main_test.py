import mox
import os
import shutil
import sys
import tempfile
import unittest
from cStringIO import StringIO

from sso_server import application
from sso_server import main


class SSOServerCommandLineTest(mox.MoxTestBase):

    def setUp(self):
        mox.MoxTestBase.setUp(self)
        self.tmpdir = tempfile.mkdtemp()
        self.mox.StubOutWithMock(application, 'create_app')

    def tearDown(self):
        mox.MoxTestBase.tearDown(self)
        shutil.rmtree(self.tmpdir)

    def make_config(self, **conf_attrs):
        test_conf_file = os.path.join(self.tmpdir, 'config')
        with open(test_conf_file, 'w') as fd:
            for key, value in conf_attrs.items():
                fd.write('%s = "%s"\n' % (key, value))
        return '--config=%s' % test_conf_file

    def run_server(self, args):
        out = StringIO()
        old_stdout, sys.stdout = sys.stdout, out
        old_stderr, sys.stderr = sys.stderr, out
        try:
            try:
                args.insert(0, '--logtostderr')
                status = main.main(args)
            except SystemExit, e:
                status = e.code
            return status, out.getvalue()
        finally:
            sys.stdout, sys.stderr = old_stdout, old_stderr

    def test_prints_usage(self):
        status, out = self.run_server(['--help'])
        self.assertEquals(0, status)
        self.assert_("Usage" in out)

    def test_run_fails_setup(self):
        application.create_app('test.conf').AndRaise(Exception('error'))
        self.mox.ReplayAll()
        status, out = self.run_server(['--config=test.conf'])
        self.assertEquals(1, status)

    def test_run_with_options(self):
        app = 'app'
        application.create_app(os.path.abspath('test.conf')).AndReturn(app)
        self.mox.StubOutWithMock(main, 'run_server')
        main.run_server(app, 10000)
        self.mox.ReplayAll()

        status, out = self.run_server(['--config=test.conf', '--port=10000'])
        self.assertEquals(0, status)

    def test_run_server_fcgi(self):
        self.mox.StubOutWithMock(main, 'autodetect_fastcgi')
        main.autodetect_fastcgi().AndReturn(True)

        import flup.server.fcgi
        self.mox.StubOutWithMock(flup.server.fcgi, 'WSGIServer', use_mock_anything=True)
        wsgi_server = self.mox.CreateMockAnything()
        flup.server.fcgi.WSGIServer('app').AndReturn(wsgi_server)
        wsgi_server.run()
        self.mox.ReplayAll()

        main.run_server('app', 1234)

    def test_run_server_http(self):
        self.mox.StubOutWithMock(main, 'autodetect_fastcgi')
        main.autodetect_fastcgi().AndReturn(False)

        server = self.mox.CreateMockAnything()
        try:
            import gevent.wsgi
            self.mox.StubOutWithMock(gevent.wsgi, 'WSGIServer', use_mock_anything=True)
            gevent.wsgi.WSGIServer(('0.0.0.0', 1234), 'app').AndReturn(server)
            server.serve_forever()
        except ImportError:
            import werkzeug
            self.mox.StubOutWithMock(werkzeug, 'run_simple')
            werkzeug.run_simple('0.0.0.0', 1234, 'app')

        self.mox.ReplayAll()

        main.run_server('app', 1234)
