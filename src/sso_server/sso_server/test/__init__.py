import os
import re
import shutil
import sso
import tempfile
import unittest
from sso_server import application


def parse_form(response):
    v = {}
    for match in re.findall(
            r'<input.*name="([^"]+)".*value="([^"]+)"', response.data):
        v[match[0]] = match[1]
    return v


class SSOServerTestBase(unittest.TestCase):

    def setUp(self):
        self.tmpdir = tempfile.mkdtemp()
        public_key, secret_key = sso.generate_keys()
        self.public_key_file = os.path.join(self.tmpdir, 'public.key')
        with open(self.public_key_file, 'w') as fd:
            fd.write(public_key)
        self.secret_key_file = os.path.join(self.tmpdir, 'secret.key')
        with open(self.secret_key_file, 'w') as fd:
            fd.write(secret_key)
        self.domain = 'testdomain'
        self.app = self._make_app()

    def _config(self):
        return {}

    def _make_app(self, **config):
        config_ = self._config()
        config_.update(config)
        return application.create_app(config=config_)

    def tearDown(self):
        shutil.rmtree(self.tmpdir)
        
    def _extract_csrf(self, data):
        m = re.search(r'<input type="hidden" name="_csrf" value="([^"]+)"', data)
        self.assertTrue(m is not None, "Could not extract CSRF\n\nPage contents:\n%s" % data)
        return m.group(1)

    def _login(self, c, location, query_args):
        query_args['d'] = query_args['d'][0].replace('http://', 'https://')
        response = c.get(location, query_string=query_args)
        self.assertEquals(200, response.status_code, response.data)
        values = parse_form(response)
        self.assertTrue('_csrf' in values, values)
        values['username'] = 'admin'
        values['password'] = 'admin'
        response = c.post('/', data=values)
        self.assertEquals(302, response.status_code, response.data)
        return response
