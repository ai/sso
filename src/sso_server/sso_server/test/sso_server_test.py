import base64
import logging
import os
import urlparse
import urllib
import cookielib
from werkzeug.utils import parse_cookie
import flask
import sso

from sso_server import application
from sso_server.test import SSOServerTestBase

logging.basicConfig()

        
def urldecode(query_string):
    return dict(urlparse.parse_qsl(query_string))


class SSOServerTest(SSOServerTestBase):

    def _config(self):
        return {
            'SSO_SECRET_KEY': self.secret_key_file,
            'SSO_PUBLIC_KEY': self.public_key_file,
            'SSO_DOMAIN': self.domain,
            'SECRET_KEY': 'barbablu',
            'ALLOWED_SERVICES': [],
            }

    def get_local_ticket(self, user):
        return self.app.login_service.local_generate(user)

    def get_auth_client(self, user='user', ticketstr=None):
        if not ticketstr:
            ticketstr = self.get_local_ticket(user)
        c = self.app.test_client()
        c.cookie_jar.set_cookie(
            cookielib.Cookie(None, application.SSO_COOKIE_NAME, ticketstr,
                             cookielib.Absent, False, '.', False, False, '/', True,
                             True, None, False, None, None, None))
        return c

    def _get_cookies(self, response):
        cookies = {}
        for x in response.headers.get_all('Set-Cookie'):
            cookies.update(parse_cookie(x))
        return cookies

    def _check_ticket(self, query_string, expected_d, expected_s, nonce=None):
        # Check the query string arguments.
        args = urldecode(query_string)
        self.assertEquals(expected_d, args.get('d'))
        self.assertEquals(expected_s, args.get('s'))
        self.assertTrue(args.get('t'))

        # Check that we got a valid ticket for the remote service.
        vrfy = sso.Verifier(self.app.login_service.public_key,
                            expected_s, self.domain)
        return vrfy.verify(args['t'], nonce)

    def test_login_ok_with_cookie(self):
        # Test that we can login successfully if we already have a local
        # authentication ticket for the SSO service.

        # Get a local ticket
        with self.get_auth_client('user') as c:
            parms = {'s': 'someservice.org/',
                     'd': 'https://someservice.org/dest'}
            response = c.get('/', query_string=parms)

            # Check that we got a 302 redirect
            self.assertEquals(302, response.status_code)
            location = response.headers['Location']
            path, qs = location.split('?', 1)
            self.assertEquals('https://someservice.org/sso_login', path)

            ticket = self._check_ticket(
                qs, 'https://someservice.org/dest', 'someservice.org/')
            self.assertEquals('user', ticket.user())

            # Check that we are tracking the active services in the session.
            active_services = flask.session.get('active_services')
            self.assertTrue(active_services is not None)
            self.assertTrue('someservice.org/' in active_services)

    def test_login_ok_with_cookie_and_groups(self):
        # Test that we can login successfully if we already have a local
        # authentication ticket for the SSO service, with group auth.

        # Get a local ticket
        with self.get_auth_client('user') as c:
            parms = {'s': 'someservice.org/',
                     'd': 'https://someservice.org/dest',
                     'g': 'group1,othergroup'}
            response = c.get('/', query_string=parms)

            # Check that we got a 302 redirect
            self.assertEquals(302, response.status_code)
            location = response.headers['Location']
            path, qs = location.split('?', 1)
            self.assertEquals('https://someservice.org/sso_login', path)

            # Check the resulting ticket.
            ticket = self._check_ticket(
                qs, 'https://someservice.org/dest', 'someservice.org/')
            self.assertEquals('user', ticket.user())
            self.assertTrue('group1' in ticket.groups())

    def test_login_fail_with_wrong_cookie(self):
        # Test that a broken SSO local authentication cookie will fail.
        with self.get_auth_client(ticketstr='something invalid') as c:
            parms = {'s': 'someservice.org/',
                     'd': 'https://someservice.org/dest'}
            response = c.get('/', query_string=parms)
            self.assertEquals(200, response.status_code)

    def test_login_fail_with_cookie_and_nonmatching_groups(self):
        # Test that non-matching groups will cause auth to fail even
        # when local authentication is ok.
        with self.get_auth_client(user='user') as c:
            parms = {'s': 'someservice.org/',
                     'd': 'https://someservice.org/dest',
                     'g': 'badgroup,otherbadgroup'}
            response = c.get('/', query_string=parms)

        # Check that we got a 403 reply
        self.assertEquals(403, response.status_code)

    def test_login_show_form(self):
        # Test that the login form is shown properly, with CSRF protection.
        with self.app.test_client() as c:
            parms = {'s': 'someservice.org/',
                     'd': 'https://someservice.org/'}
            response = c.get('/', query_string=parms)
            self.assertEquals(200, response.status_code)
            self.assertTrue('<form' in response.data)
            self.assertTrue('name="username"' in response.data)
            self.assertTrue('name="password"' in response.data)
            self.assertTrue("someservice.org" in response.data)

    def test_login_form_csrf(self):
        with self.app.test_client() as c:
            parms = {'s': 'someservice.org/',
                     'd': 'https://someservice.org/'}
            response = c.get('/', query_string=parms)
            # Check that the CSRF tokens match.
            csrf_html = self._extract_csrf(response.data)
            csrf_session = flask.session['_csrf']
            self.assertEquals(csrf_html, csrf_session)

    def test_login_show_form_with_groups(self):
        # Test that group information is correctly present in the login form.
        with self.app.test_client() as c:
            parms = {'s': 'someservice.org/',
                     'd': 'https://someservice.org/',
                     'g': 'mygroup'}
            response = c.get('/', query_string=parms)
            self.assertEquals(200, response.status_code)
            self.assertTrue('name="g" value="mygroup"' in response.data)

    def test_login_ok_with_form(self):
        # Test that we can successfully login via the login form.
        with self.app.test_client() as c:
            parms = {'s': 'someservice.org/',
                     'd': 'https://someservice.org/dest'}
            response = c.get('/', query_string=parms)
            self.assertEquals(200, response.status_code)
            csrf_token = self._extract_csrf(response.data)

            parms.update({'username': 'admin',
                          'password': 'admin',
                          '_csrf': csrf_token})
            response = c.post('/', data=parms)
            self.assertEquals(302, response.status_code, response.data)
            location = response.headers['Location']
            path, qs = location.split('?', 1)
            self.assertEquals('https://someservice.org/sso_login', path)

            ticket = self._check_ticket(
                qs, 'https://someservice.org/dest', 'someservice.org/')
            self.assertEquals('admin', ticket.user())

    def test_login_ok_with_form_utf8(self):
        # Test that the login form understands non-ASCII parameters.
        with self.app.test_client() as c:
            parms = {'s': 'someservice.org/',
                     'd': 'https://someservice.org/dest'}
            response = c.get('/', query_string=parms)
            self.assertEquals(200, response.status_code)
            csrf_token = self._extract_csrf(response.data)

            u = u'\xc3dmin'
            parms.update({'username': u,
                          'password': u,
                          '_csrf': csrf_token})
            response = c.post('/', data=parms)
            self.assertEquals(302, response.status_code, response.data)
            location = response.headers['Location']
            path, qs = location.split('?', 1)
            self.assertEquals('https://someservice.org/sso_login', path)

            ticket = self._check_ticket(
                qs, 'https://someservice.org/dest', 'someservice.org/')
            self.assertEquals(u, unicode(ticket.user(), 'utf-8'))

    def test_login_ok_with_form_and_nonce(self):
        # Test that we can successfully login via the login form,
        # including a nonce in the process.
        with self.app.test_client() as c:
            parms = {'s': 'someservice.org/',
                     'd': 'https://someservice.org/dest',
                     'n': 'testnonce'}
            response = c.get('/', query_string=parms)
            self.assertEquals(200, response.status_code)
            csrf_token = self._extract_csrf(response.data)

            parms.update({'username': 'admin',
                          'password': 'admin',
                          '_csrf': csrf_token})
            response = c.post('/', data=parms)
            self.assertEquals(302, response.status_code, response.data)
            location = response.headers['Location']
            path, qs = location.split('?', 1)
            self.assertEquals('https://someservice.org/sso_login', path)
            ticket = self._check_ticket(
                qs, 'https://someservice.org/dest', 'someservice.org/',
                'testnonce')
            self.assertEquals('admin', ticket.user())
            self.assertEquals('testnonce', ticket.nonce())

    def test_login_fail_auth(self):
        # Test authentication failure on the login form.
        with self.app.test_client() as c:
            parms = {'s': 'someservice.org/',
                     'd': 'https://someservice.org/dest'}
            response = c.get('/', query_string=parms)
            self.assertEquals(200, response.status_code)
            csrf_token = self._extract_csrf(response.data)
            
            parms.update({'username': 'admin',
                          'password': 'not_admin',
                          '_csrf': csrf_token})
            response = c.post('/', data=parms)
            self.assertEquals(200, response.status_code, response.data)
            self.assertTrue('Authentication failed' in response.data)

    def test_login_fail_csrf(self):
        # Test CSRF failure on the login form.
        with self.app.test_client() as c:
            parms = {'s': 'someservice.org/',
                     'd': 'https://someservice.org/dest'}
            response = c.get('/', query_string=parms)
            self.assertEquals(200, response.status_code)

            parms.update({'username': 'admin',
                          'password': 'admin',
                          '_csrf': 'blah'})
            response = c.post('/', data=parms)
            self.assertEquals(200, response.status_code, response.data)
            self.assertTrue('Invalid session' in response.data)

    def test_login_params_validation(self):
        # Test validation of service/destination pairs.
        sdvars = [('aa.com/', 'https://aa.com/'),
                  ('aa.com/', 'http://aa.com/'),
                  ('aa.bb.cc.com/', 'https://aa.bb.cc.com/'),
                  ('s-d.001.net/', 'https://s-d.001.net/'),
                  ('aa.com/test/', 'https://aa.com/test/'),
                  ('aa.com/', 'https://aa.com/test/'),
                  ('XX320.EU.something.com/', 'https://XX320.EU.something.com/'),
                  ('something.com:8443/', 'https://something.com:8443/'),
                  ]
        localtkt = self.get_local_ticket('user')
        for s, d in sdvars:
            with self.get_auth_client(ticketstr=localtkt) as c:
                response = c.get('/', query_string={'s': s, 'd': d})
                self.assertEquals(302, response.status_code,
                                  'sso failure for s=%s, d=%s: status=%d, expected=302' % (
                        s, d, response.status_code))
                location = response.headers['Location'].split('?')[0]
                location_exp = 'https://%ssso_login' % s
                self.assertEquals(location_exp, location,
                                  'redirect failure for s=%s, d=%s: location=%s, expected=%s' % (
                        s, d, location, location_exp))

    def test_login_fail_params_validation(self):
        # Test validation of incorrect service/destination pairs.
        sdvars = [('notafqdn/', 'https://notafqdn/', 403),
                  ('notafqdn/zz/', 'https://notafqdn/zz/', 403),
                  ('noendingslash', 'https://noendingslash', 403),
                  ('aa.com/something/', 'https://aa.com/', 403),
                  ('aa.com/something/', 'https://aa.com/somethingelse/', 403),
                  ('something/', '', 200),
                  ('', 'https://something/', 200),
                  ('something.com:443/', 'https://something.com/', 403),
                  ('something.com:8443/', 'https://something.com:2121/', 403),
                  ('SOMETHING.com/', 'https://something.com/', 403),
                  ('something.com/', 'ftp://something.com/', 403),
                  ]
        localtkt = self.get_local_ticket('user')
        for s, d, exp_status in sdvars:
            with self.get_auth_client(ticketstr=localtkt) as c:
                response = c.get('/', query_string={'s': s, 'd': d})
                self.assertEquals(exp_status, response.status_code, 
                                  'bad request returned %d (exp. %d): s=%s, d=%s' % (
                        response.status_code, exp_status, s, d))

    def test_allowed_services(self):
        # Verify that ALLOWED_SERVICES is correctly applied.
        self.app = self._make_app(ALLOWED_SERVICES=['^some.*\.com/$'])
        sdvars = [('something.com/', 'https://something.com/blah', 302),
                  ('somethingelse.com/', 'https://somethingelse.com/', 302),
                  ('badthing.com/', 'https://badthing.com/', 403)]

        localtkt = self.get_local_ticket('user')
        for s, d, exp_status in sdvars:
            with self.get_auth_client(ticketstr=localtkt) as c:
                response = c.get('/', query_string={'s': s, 'd': d})
                self.assertEquals(exp_status, response.status_code, 
                                  'bad request returned %d (exp. %d): s=%s, d=%s' % (
                        response.status_code, exp_status, s, d))

    def test_authentication_module_failure(self):
        # Test that an application error in the auth module won't cause
        # the app to crash, but will return an authentication failure.
        with self.app.test_client() as c:
            parms = {'s': 'someservice.org/',
                     'd': 'https://someservice.org/dest'}
            response = c.get('/', query_string=parms)
            self.assertEquals(200, response.status_code)
            csrf_token = self._extract_csrf(response.data)

            parms.update({'username': 'error',
                          'password': 'error',
                          '_csrf': csrf_token})
            response = c.post('/', data=parms)
            self.assertEquals(200, response.status_code, response.data)
            self.assertTrue('Authentication failed' in response.data)

    def test_logout_nop(self):
        # Test logging out without having logged in.
        with self.app.test_client() as c:
            response = c.get('/logout')
            self.assertEquals(200, response.status_code)

    def test_logout_ok(self):
        # Test logging out from multiple services.
        with self.get_auth_client() as c:
            response = c.get('/', query_string={'s': 'svc1.com/', 'd': 'https://svc1.com/'})
            self.assertEquals(302, response.status_code)
            response = c.get('/', query_string={'s': 'svc2.com/', 'd': 'https://svc2.com/'})
            self.assertEquals(302, response.status_code)

            response = c.get('/logout')
            self.assertEquals(200, response.status_code)

            # Verify that cookies have been wiped.
            cookies = self._get_cookies(response)
            self.assertFalse(cookies.get(application.SSO_COOKIE_NAME))

            # Verify that the result page links to service-specific logouts.
            self.assertTrue('https://svc1.com/sso_logout' in response.data)
            self.assertTrue('https://svc2.com/sso_logout' in response.data)
        
    def test_static_data(self):
        with self.app.test_client() as c:
            response = c.get('/static/pure.min.css')
            self.assertEquals(200, response.status_code)
            self.assertTrue(response.headers['Content-Type'].startswith('text/css'))

    def test_notfound(self):
        with self.app.test_client() as c:
            response = c.get('/notfound')
            self.assertEquals(404, response.status_code)

    def test_cache_headers(self):
        with self.app.test_client() as c:
            response = c.get('/')
            self.assertEquals('-1', response.headers['Expires'])
            self.assertEquals('no-cache', response.headers['Cache-Control'])

    def test_xss_headers(self):
        with self.app.test_client() as c:
            response = c.get('/')
            self.assertEquals('SAMEORIGIN', response.headers['X-Frame-Options'])
            self.assertEquals('1; mode=block', response.headers['X-XSS-Protection'])


if __name__ == "__main__":
    unittest.main()
