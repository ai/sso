import base64
import logging
import os
import sso
import urllib
import urlparse
import zlib
from datetime import datetime

from sso_server.test import SSOServerTestBase

logging.basicConfig()


def parse_args(url):
    up = urlparse.urlparse(url)
    return up.path, urlparse.parse_qs(up.query)


class SAMLTest(SSOServerTestBase):

    def setUp(self):
        super(SAMLTest, self).setUp()
        self.app = self._make_app()

    def _config(self):
        saml_cert = os.path.join(os.path.dirname(__file__), 'saml.pem')
        saml_key = os.path.join(os.path.dirname(__file__), 'saml.key')
        return {
            'SSO_SECRET_KEY': self.secret_key_file,
            'SSO_PUBLIC_KEY': self.public_key_file,
            'SSO_DOMAIN': self.domain,
            'SECRET_KEY': 'barbablu',
            'ALLOWED_SERVICES': ['localhost/saml/'],
            'SAML': {
                'SSO_LOGIN_SERVER': 'localhost',
                'CERTIFICATE_FILE': saml_cert,
                'PRIVATE_KEY_FILE': saml_key,
                'SAML2IDP_REMOTES': {
                    'test': {
                        'acs_url': 'https://saml.example.com/users/auth/saml/callback',
                        'processor': 'sso_server.saml.registry.SSOProcessor',
                    },
                },
            },
        }

    def test_idp_metadata(self):
        # Fetch the IDP XML metadata, to verify that the SAML
        # blueprint is properly plugged into the main app.
        with self.app.test_client() as c:
            response = c.get('/saml/metadata/xml/')
            self.assertEquals(200, response.status_code)

    def _make_saml_request(self):
        saml_request = "<samlp:AuthnRequest AssertionConsumerServiceURL='https://saml.example.com/users/auth/saml/callback' Destination='https://localhost/saml/login' ID='_14443901-284e-4780-ae25-686f6fd781aa' IssueInstant='%(stamp)s' Version='2.0' xmlns:saml='urn:oasis:names:tc:SAML:2.0:assertion' xmlns:samlp='urn:oasis:names:tc:SAML:2.0:protocol'><saml:Issuer>https://saml.example.com</saml:Issuer><samlp:NameIDPolicy AllowCreate='true' Format='urn:oasis:names:tc:SAML:2.0:nameid-format:transient'/></samlp:AuthnRequest>" % {
            'stamp': datetime.now().isoformat(),
            }
        comp = zlib.compressobj(9, zlib.DEFLATED, -15)
        comp.compress(saml_request)
        return base64.b64encode(comp.flush())

    def test_saml_login_empty_request(self):
        with self.app.test_client() as c:
            response = c.get('/saml/login')
            self.assertEquals(400, response.status_code)

    def test_saml_login(self):
        with self.app.test_client() as c:

            def _follow(path, data):
                response = c.get(path, query_string=data)
                self.assertEquals(302, response.status_code,
                                  'got %d for %s, expecting 302' % (
                                      response.status_code, path))
                path, data = parse_args(response.location)
                return path, data
            
            saml_request = self._make_saml_request()
            path, data = _follow('/saml/login', {'SAMLRequest': saml_request})
            # Follow the redirect to /login/process.
            path, data = _follow(path, data)
            # Submit the login form.
            self.assertEquals('/', path)
            response = self._login(c, path, data)
            # Now request the /sso_login endpoint.
            path, data = parse_args(response.location)
            self.assertEquals('/saml/sso_login', path)
            # Follow to the sso_login endpoint
            path, data = _follow(path, data)
            self.assertEquals('/saml/login/process', path)
            # Back to the SAML app at last
            # Finally we're getting some xml thingy or what            
            response = c.get(path, query_string=data)
            self.assertEquals(200, response.status_code)
