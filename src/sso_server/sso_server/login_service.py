import logging
import re
import sso
import sys
import urlparse

log = logging.getLogger(__name__)


class Error(Exception):
    pass


class AccessError(Error):
    pass


class SetupError(Error):
    pass


class LoginService(object):
    """Single sign-on service.

    The service can be configured with the following parameters (which
    are taken from the Flask application config):

    SSO_SECRET_KEY
      Location of the file with the main SSO secret key.

    SSO_PUBLIC_KEY
      Location of the file with the main SSO public key.

    SSO_DOMAIN
      SSO domain (this currently acts as a shared secret).

    SSO_ENABLE_OTP
      Enable (T)OTP for non-SSO authentication requests.

    SSO_AUTH_MODULE
      Authentication plugin. This should be the Python module path to
      something which implements the sso_server.auth.AuthBase interface.

    ALLOWED_SERVICES
      A list of regular expression patterns defining the services
      which are allowed. If left unset (the default), no checks will
      be performed on the destination service upon authentication,
      which is *only* useful for testing purposes. Production instances
      should define this variable.

    LOGIN_TICKET_TTL
      Time to live for the main SSO login ticket (in seconds).

    SERVICE_TICKET_TTL
      Time to live for service-specific SSO tickets (in seconds).

    """

    SERVICE_PATTERN = re.compile(
        r'^(?:(?:[a-z0-9][-a-z0-9]*\.)+[a-z]{2,4}|localhost)(?::[0-9]{2,5})?(?:/.*)?/$',
        re.IGNORECASE)

    LOGIN_SERVICE = '_login'

    DEFAULT_LOGIN_TICKET_TTL = 86400

    DEFAULT_SERVICE_TICKET_TTL = 1800

    def __init__(self, config):
        with open(config['SSO_SECRET_KEY']) as fd:
            self.secret_key = fd.read()
        with open(config['SSO_PUBLIC_KEY']) as fd:
            self.public_key = fd.read()
        self.signer = sso.Signer(self.secret_key)
        self.domain = config['SSO_DOMAIN']
        self.local_verifier = sso.Verifier(
            self.public_key, self.LOGIN_SERVICE, self.domain)

        self.allowed_services = [
            re.compile(x) for x in config.get('ALLOWED_SERVICES', [])]

        self.login_ticket_ttl = config.get(
            'LOGIN_TICKET_TTL', self.DEFAULT_LOGIN_TICKET_TTL)
        self.service_ticket_ttl = config.get(
            'SERVICE_TICKET_TTL', self.DEFAULT_SERVICE_TICKET_TTL)

        __import__(config['SSO_AUTH_MODULE'])
        self.auth = sys.modules[config['SSO_AUTH_MODULE']].Auth(config)
        self.enable_otp = (config.get('SSO_ENABLE_OTP', False)
                           and self.auth.supports_otp)

        # Run a sanity check before returning.
        self._sanity_check()

    def _sanity_check(self):
        try:
            tktstr = self.local_generate('__test')
            ticket = self.local_verifier.verify(tktstr)
        except sso.Error, e:
            raise SetupError('Sanity check failure: Unable to correctly '
                             'generate a signed ticket: %s' % str(e))

    def local_authorize(self, ticket_string):
        """Authorize locally (against the service)."""
        try:
            return self.local_verifier.verify(ticket_string)
        except sso.Error:
            return None

    def local_generate(self, username):
        """Generate a local authorization ticket."""
        ticket = sso.Ticket(username,
                            self.LOGIN_SERVICE,
                            self.domain,
                            ttl=self.login_ticket_ttl)
        return self.signer.sign(ticket)

    def authorize(self, username, service, destination, nonce, required_groups):
        """Authorize authentication to a remote service.

        Args:
          username: username
          service: service that is requesting authorization
          destination: requested destination
          nonce: unique nonce for this authentication
          required_groups: groups required by requester (optional)

        Returns:
          A signed ticket string.

        Raises:
          AccessError when validation fails. This is a permanent error.
        """
        self._validate_service_access(service, destination)
        groups = self._validate_group_access(username, required_groups)
        ticket = sso.Ticket(username, service, self.domain,
                            nonce=nonce,
                            groups=groups,
                            ttl=self.service_ticket_ttl)
        return self.signer.sign(ticket)

    def _validate_service_access(self, service, destination):
        """Check that service and destination are ok."""
        if not service or not destination:
            raise AccessError('No service or destination')

        if not self.SERVICE_PATTERN.search(service):
            raise AccessError('Bad service')

        if self.allowed_services:
            for pattern in self.allowed_services:
                if pattern.search(service):
                    break
            else:
                raise AccessError('Service not allowed')

        scheme, netloc, path, query, frag = urlparse.urlsplit(destination)
        if scheme not in ('http', 'https'):
            raise AccessError('Unsupported URL scheme')
        netloc_path = netloc + path
        if not netloc_path.startswith(service):
            raise AccessError('Destination not allowed')

    def _validate_group_access(self, username, required_groups):
        if required_groups:
            matching = self.auth.match_groups(username, required_groups)
            if not matching:
                raise AccessError('No matching group membership')
            return matching
        else:
            return set()

    def authenticate(self, username, password, otp=None):
        """Authenticate a user."""
        try:
            if self.enable_otp:
                return self.auth.authenticate(username, password, otp)
            else:
                return self.auth.authenticate(username, password)
        except:
            log.exception('Unhandled exception in authenticate()')
