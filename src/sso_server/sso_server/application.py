import functools
import json
import logging
import re
import urllib
import uuid
from flask import Flask, redirect, request, session, render_template, make_response
from sso_server import login_service

app = Flask('sso_server')

SSO_COOKIE_NAME = '_sso_tkt'


# The sso library wants basestring objects, so just encode all
# incoming unicode objects to utf-8.
def _tostr(s):
    if isinstance(s, unicode):
        return s.encode('utf-8', 'replace')
    else:
        return s


@app.after_request
def set_headers(response):
    response.cache_control.no_cache = True
    response.headers['Expires'] = '-1'
    response.headers['X-XSS-Protection'] = '1; mode=block'
    response.headers['X-Frame-Options'] = 'SAMEORIGIN'
    return response


class LoginParams(object):

    def __init__(self, req):
        if req.method == 'GET':
            form = req.args
        else:
            form = req.form
        self.service = _tostr(form.get('s'))
        self.destination = _tostr(form.get('d'))
        self.nonce = _tostr(form.get('n'))
        self.groups = _tostr(form.get('g'))
        if self.groups:
            self.groups = set(self.groups.split(','))

    def valid(self):
        return (self.service and self.destination)


def show_info_page():
    return render_template(
        'info.html',
        services=session.get('active_services', []))


def show_login_page(params, err=None):
    csrf_token = str(uuid.uuid4())
    session['_csrf'] = csrf_token
    return render_template(
        'login.html',
        service=params.service,
        destination=params.destination,
        nonce=params.nonce or '',
        groups=params.groups or [],
        csrf_token=csrf_token,
        enable_otp=app.login_service.enable_otp,
        err=err)


def show_unauthorized_page(err):
    return make_response(
        render_template('unauthorized.html', err=err), 403)


@app.route('/', methods=('GET', 'POST'))
def login():
    params = LoginParams(request)

    # Redirect to the info page if no parameters are given.
    if not params.valid():
        return show_info_page()

    # Extract local auth cookie if present. If not, show the login
    # form.
    local_ticket = None
    local_ticket_str = request.cookies.get(SSO_COOKIE_NAME)
    if local_ticket_str:
        local_ticket = app.login_service.local_authorize(
            _tostr(local_ticket_str))

    if request.method == 'GET':
        # Check local authentication.
        if not local_ticket:
            return show_login_page(params)

        username = local_ticket.user()
    else:
        # Form submission, validate CSRF, username and password.
        csrf_token = session.pop('_csrf', None)
        if not csrf_token or csrf_token != request.form.get('_csrf'):
            app.logger.error(
                'CSRF failure, session=%s, form=%s', csrf_token,
                request.form.get('_csrf'))
            return show_login_page(params, 'Invalid session')
        username = _tostr(request.form.get('username'))
        password = _tostr(request.form.get('password'))
        otp = None
        if app.login_service.enable_otp:
            otp = _tostr(request.form.get('otp'))
        if not app.login_service.authenticate(username, password, otp):
            app.logger.error(
                'authentication failure for user %s', username)
            # Authentication failed, show login page again.
            return show_login_page(params, 'Authentication failed')

        # Set local auth cookie.
        local_ticket_str = app.login_service.local_generate(username)

    # At this point the user is authenticated, check authorization
    # and create the single sign-on ticket.
    try:
        ticket_str = app.login_service.authorize(
            username, params.service, params.destination,
            params.nonce, params.groups)
    except login_service.Error, e:
        app.logger.error(
            'unauthorized access for user %s: service=%s, dest=%s, '
            'err=%s', username, params.service, params.destination, str(e))
        return show_unauthorized_page(e)

    # Keep track of the active services.
    active_services = set(session.get('active_services', []))
    active_services.add(params.service)
    session['active_services'] = list(active_services)

    # Ensure that the session stays around for a while.
    session.permanent = True

    # Create redirect response, setting all the right cookies.
    # FIXME: detect scheme from destination
    scheme = 'https'
    login_endpoint = '%s://%ssso_login' % (scheme, params.service)
    redirect_url = '%s?%s' % (
        login_endpoint, urllib.urlencode({'t': ticket_str,
                                          'd': params.destination,
                                          's': params.service}))

    response = make_response(redirect(redirect_url))
    if not local_ticket:
        response.set_cookie(SSO_COOKIE_NAME, local_ticket_str,
                            path=app.config['APPLICATION_ROOT'] or '/',
                            secure=True, httponly=True)
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response


@app.route('/logout')
def logout():
    active_services = session.pop('active_services', [])
    response = make_response(
        render_template('logout.html', services=active_services))
    response.delete_cookie(SSO_COOKIE_NAME)
    return response


def create_app(config_file=None, config={}):
    # Some defaults for our configuration.
    app.config.update({
        'SSO_SECRET_KEY': '/etc/sso/secret.key',
        'SSO_PUBLIC_KEY': '/etc/sso/public.key',
        'SSO_AUTH_MODULE': 'sso_server.auth.auth_test',
        'HTML_TITLE': 'SSO',
        'HTML_BANNER': 'Single Sign-On',
    })

    # Set saner defaults for Flask session config.
    app.config.update({
        'SESSION_COOKIE_HTTPONLY': True,
        'SESSION_COOKIE_SECURE': True,
    })

    # Load the user-provided configuration.
    app.config.update(config)
    if config_file:
        app.config.from_pyfile(config_file)
    else:
        app.config.from_envvar('APP_CONFIG', silent=True)

    # Check configuration for required attributes.
    required_attrs = ['SSO_DOMAIN', 'SSO_SECRET_KEY', 'SSO_PUBLIC_KEY',
                      'SSO_AUTH_MODULE']
    missing_attrs = [x for x in required_attrs if not app.config.get(x)]
    if missing_attrs:
        raise Exception('Missing required configuration attributes: %s',
                        ', '.join(missing_attrs))

    # Optionally enable Sentry.
    if 'SENTRY_DSN' in app.config:
        try:
            from raven.contrib.flask import Sentry
            sentry = Sentry(app)
        except ImportError:
            pass

    # Optionally enable SAML handlers.
    if 'SAML' in app.config:
        from sso_server.saml.flask_views import init_app as saml_init_app
        saml_init_app(app)
        # Automatically add it to the allowed services.
        if app.config.get('ALLOWED_SERVICES'):
            app.config['ALLOWED_SERVICES'].append(
                '%s/saml/' % app.config['SAML']['SSO_LOGIN_SERVER'].replace('.', r'\.'))

    # Initialize the login service.
    app.login_service = login_service.LoginService(app.config)

    return app
