# Copyright (c) 2008 Autistici/Inventati <info@autistici.org>
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

import ldap
from ldap.dn import escape_dn_chars
import logging
import re
from sso_server.auth import AuthBase

log = logging.getLogger(__name__)


class Auth(AuthBase):
    """LDAP Authentication.

    The module will attempt to bind as the user with the given credentials.
    It can be configured with a number of options:

    AUTH_LDAP_URI
        The URI of the LDAP server to connect to 
        (default: ldap://localhost:389).

    AUTH_LDAP_BASE
        Base DN to build the user DN.  You will need to specify this.

    AUTH_LDAP_RDN_FORMAT
        How to build the RDN (default: uid=%s). The result will be
        prepended to AUTH_LDAP_BASE to generate the final DN. The
        "%s" token will be replaced by the username.
    """

    def __init__(self, config):
        self.base = config['AUTH_LDAP_BASE']
        self.uri = config.get('AUTH_LDAP_URI', 'ldap://localhost:389')
        self.rdn_fmt = config.get('AUTH_LDAP_RDN_FORMAT', 'uid=%s')

    def authenticate(self, username, password, otp=None):
        if not username or not password:
            return False
        rdn = self.rdn_fmt % escape_dn_chars(username)
        dn = '%s,%s' % (rdn, self.base)
        l = ldap.ldapobject.LDAPObject(self.uri)
        result = False
        try:
            l.simple_bind_s(dn, password)
            result = True
        except ldap.INVALID_CREDENTIALS:
            pass
        except Exception as e:
            log.error('LDAP error while authenticating %s: %s', username, e)
        l.unbind()
        return result

    def match_groups(self, username, groups):
        # TODO: Not implemented. There are too many ways to store
        # group membership information in LDAP.
        return set()
