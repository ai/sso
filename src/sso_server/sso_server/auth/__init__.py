# Copyright (c) 2008 Autistici/Inventati <info@autistici.org>
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.


class AuthBase(object):
    """Base class for an authentication provider."""

    # Set to True if this auth provider supports 2FA (via OTP tokens).
    supports_otp = False

    def __init__(self, config):
        pass

    def authenticate(self, username, password, otp=None):
        """Authenticate a user.

        Args:
          username: username (utf-8 encoded string).
          password: password (utf-8 encoded string).
          otp: OTP token, if provided by the user. Note that this
              should be a string, not an integer.
        Returns:
          A boolean result indicating successful authentication.
        """
        return False

    def match_groups(self, username, groups):
        """Match user groups against requested ones.

        Args:
          username: username (utf-8 encoded string).
          groups: collection (list, set) of requested groups. Group
              names are strings.
        Returns:
          An unsorted collection (set) containing the intersection of
          the groups that the user is a member of and the requested
          groups.
        """
        return set()

    def get_user_email(self, username):
        """Return the email address associated with a user.

        The SAML module requires this information: some endpoints
        (Gitlab, for instance) make use of it to match local records.

        Args:
          username: username (utf-8 encoded string).
        Returns:
          The email address associated with the user, or None.
        """
        return None
