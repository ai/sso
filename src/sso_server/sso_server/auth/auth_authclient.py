# Copyright (c) 2016 Autistici/Inventati <info@autistici.org>
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

import authclient
from sso_server.auth import AuthBase


class Auth(AuthBase):
    """Authclient authentication provider.

    This module knows about the following options:

    AUTHCLIENT_SERVER_URL
        The URI of the authserver.

    AUTHCLIENT_CERT
        File with the SSL client certificate.

    AUTHCLIENT_KEY
        File with the SSL client private key.

    AUTHCLIENT_SERVICE
        Service name for the authclient protocol.

    AUTHCLIENT_SHARD
        Shard for the authclient protocol (optional).

    """

    supports_otp = True

    def __init__(self, config):
        self._client = authclient.Client(
            url=config['AUTHCLIENT_SERVER_URL'],
            client_cert=config['AUTHCLIENT_CERT'],
            client_key=config['AUTHCLIENT_KEY'],
        )
        self._service = config['AUTHCLIENT_SERVICE']
        self._shard = config.get('AUTHCLIENT_SHARD')

    def authenticate(self, username, password, otp=None):
        result = self._client.authenticate(
            self._service,
            username, password, otp,
            shard=self._shard)
        return result == authclient.OK
