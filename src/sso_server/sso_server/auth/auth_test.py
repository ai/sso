# Copyright (c) 2008 Autistici/Inventati <info@autistici.org>
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

from sso_server.auth import AuthBase


class Auth(AuthBase):
    """Test authenticator.

    The test authenticator will authorize you only if the username
    and password are equal. It is used only for testing purposes.
    The user will be granted membership to a set of sample groups.
    """
    def authenticate(self, u, p, otp=None):
        if u == 'error':
            raise KeyError('blah!')
        return (u and u == p)

    def match_groups(self, u, groups):
        allowed_groups = set(["group1", "group2"])
        allowed_groups.intersection_update(groups)
        return allowed_groups

    def get_user_email(self, u):
        return u + '@example.com'
