import crypt
import logging
import os
import threading
import time
import traceback
from sso_server.oath import accept_totp
from sso_server.auth import AuthBase
import machdb.client.api as mdb

log = logging.getLogger(__name__)


class _CredentialsCache(dict):

    def __init__(self):
        self._lock = threading.Lock()
        self._data = {'pw': {}, 'otp': {}, 'grp': {}}

    def update(self, pwcache, otpcache, grpcache, mailcache):
        with self._lock:
            self._data['pw'] = pwcache
            self._data['otp'] = otpcache
            self._data['grp'] = grpcache
            self._data['mail'] = mailcache

    def get(self, tag, key, default=None):
        with self._lock:
            return self._data[tag].get(key, default)


class Updater(threading.Thread):

    def __init__(self, auth_cache):
        self.auth_cache = auth_cache
        threading.Thread.__init__(self)

    def run(self):
        while True:
            try:
                self.update_auth_cache()
            except Exception, e:
                log.error('Error updating data from MachDB: %s\n%s' % (
                    str(e), traceback.format_exc()))
            time.sleep(600)

    def update_auth_cache(self):
        pwcache, otpcache, grpcache, mailcache = {}, {}, {}, {}
        for user in mdb.User.find():
            if not user.enabled:
                continue
            pwcache[user.name] = user.password
            if user.totp_key:
                otpcache[user.name] = user.totp_key
            grpcache[user.name] = set(x.name for x in user.groups)
            if user.email:
                mailcache[user.name] = user.email
        self.auth_cache.update(pwcache, otpcache, grpcache, mailcache)


class Auth(AuthBase):

    supports_otp = True

    def __init__(self, config):
        # Disable debug logging from pyactiveresource.
        logging.getLogger('pyactiveresource').setLevel(logging.ERROR)

        # Initialize the MachDB client.
        mdb.init()

        # Setup the cache and start a background thread to update it.
        self.auth_cache = _CredentialsCache()
        updater = Updater(self.auth_cache)
        updater.setDaemon(True)
        updater.start()

    def authenticate(self, username, password, otp=None):
        enc_pw = self.auth_cache.get('pw', username, 'x')
        totp_key = self.auth_cache.get('otp', username)
        if crypt.crypt(password, enc_pw) == enc_pw:
            if totp_key:
                ok, drift = accept_totp(totp_key, otp or '', format='dec6',
                                        period=30, forward_drift=2,
                                        backward_drift=2)
                return ok
            return True
        return False

    def match_groups(self, username, groups):
        user_groups = self.auth_cache.get('grp', username, set())
        user_groups.intersection_update(groups)
        return user_groups

    def get_user_email(self, username):
        return self.auth_cache.get('mail', username)

