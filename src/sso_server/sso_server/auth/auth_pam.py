# Copyright (c) 2008 Autistici/Inventati <info@autistici.org>
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

import PAM
import logging
import pwd
import grp
from sso_server.auth import AuthBase

log = logging.getLogger(__name__)


def get_user_groups(username):
    pwent = pwd.getpwnam(username)
    groups = set([grp.getgrgid(pwent[3])[0]])
    for grent in grp.getgrall():
        if username in grent[3]:
            groups.add(grent[0])
    return groups


class Auth(AuthBase):
    """PAM-based authentication.

    Uses PAM to authenticate users. The default service is 'sso', but you can
    use a different one specifying the AUTH_PAM_SERVICE variable in the
    configuration.

    """

    def __init__(self, config):
        self.service = config.get('AUTH_PAM_SERVICE', 'sso')

    def authenticate(self, username, password, otp=None):
        pam = PAM.pam()
        pam.start(self.service)
        pam.set_item(PAM.PAM_USER, username)
        def fake_conv(auth, queryList, userdata):
            return [(password, 0)]
        pam.set_item(PAM.PAM_CONV, fake_conv)
        try:
            pam.authenticate()
            pam.acct_mgmt()
            return True
        except PAM.error, resp:
            log.error('PAM auth failed: %s', resp)
            return False

    def match_groups(self, username, groups):
        user_groups = get_user_groups(username)
        return user_groups.intersection(groups)
