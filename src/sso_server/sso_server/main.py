#!/usr/bin/python
#
# A/I SSO server implementation, Python version.
#
# Copyright (c) 2008 Autistici/Inventati <info@autistici.org>
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

__version__ = '0.3'

import errno
import logging
import logging.handlers
import os
import optparse
import signal
import socket
import sys
from sso_server import application

log = logging.getLogger(__name__)


def setup_logging(opts):
    level = opts.debug and logging.DEBUG or logging.INFO
    if opts.logtostderr:
        logging.basicConfig(level=level)
    else:
        root_logger = logging.getLogger()
        root_logger.setLevel(level)
        syslog_handler = logging.handlers.SysLogHandler(address='/dev/log')
        syslog_handler.setFormatter(
            logging.Formatter('sso_server: %(name)s: %(levelname)s %(message)s'))
        root_logger.addHandler(syslog_handler)


def autodetect_fastcgi():
    # Autodetect FastCGI environments.
    try:
        s = socket.fromfd(sys.stdin.fileno(), socket.AF_INET,
                          socket.SOCK_STREAM)
        s.getpeername()
    except socket.error, (err, errmsg):
        if err != errno.ENOTCONN:
            return False
    return True


def run_server(app, port):
    # Try a number of deployment strategies.
    if autodetect_fastcgi():
        # Use 'flup' for serving FastCGI.
        from flup.server.fcgi import WSGIServer
        WSGIServer(app).run()
    else:
        log.info('starting SSO server on port %d', port)
        try:
            # Try gevent.
            from gevent.wsgi import WSGIServer
            WSGIServer(('0.0.0.0', port), app).serve_forever()
        except ImportError:
            # Fallback to the simple werkzeug HTTP server.
            from werkzeug import run_simple
            run_simple('0.0.0.0', port, app)


def main(testargs=None):
    parser = optparse.OptionParser(prog='sso_server',
                                   version='%%prog v%s' % __version__)
    parser.add_option('-c', '--config',
                      dest='config_file',
                      default='/etc/sso/config',
                      help='Configuration file (default: %default)')
    parser.add_option('-p', '--port',
                      dest='port',
                      type='int',
                      default=9600,
                      help='TCP port to listen on (default: %default)')
    parser.add_option('-d', '--debug',
                      dest='debug',
                      action='store_true')
    parser.add_option('--logtostderr',
                      dest='logtostderr',
                      action='store_true',
                      help='Log to stderr (default: syslog)')
    opts, args = parser.parse_args(testargs)
    setup_logging(opts)

    try:
        app = application.create_app(os.path.abspath(opts.config_file))
    except Exception, e:
        log.fatal('Setup error: %s', e)
        return 1

    def _exit_handler(signo, frame):
        log.info('SSO server exiting, signal %d', signo)
        sys.exit(0)
    signal.signal(signal.SIGTERM, _exit_handler)
    signal.signal(signal.SIGINT, _exit_handler)

    try:
        run_server(app, opts.port)
        return 0
    except Exception, e:
        log.exception('unhandled exception')
        return 1


if __name__ == '__main__':
    main()

