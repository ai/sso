#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "base64.h"
#include "sso.h"
#include "tweetnacl.h"

#define FIELD_SEP_STR "|"
#define FIELD_SEP_CH '|'
#define GROUP_SEP_STR ","
#define GROUP_SEP_CH ','

static char **group_list_dup(const char **groups) {
  int i = 0;
  const char **p;
  char **g, **result;

  if (groups == NULL) {
    return NULL;
  }
  /* Find size first. */
  for (i = 0, p = groups; *p; i++, p++)
    ;
  /* Allocate and deep copy. */
  result = (char **)malloc(sizeof(char *) * (i + 1));
  for (p = groups, g = result; *p; p++, g++) {
    *g = strdup(*p);
  }
  *g = NULL;
  return result;
}

static void group_list_free(char **groups) {
    char **p = groups;
    for (; *p; p++) {
      free((char *)*p);
    }
    free(groups);
}

static int group_list_overlap(char **a, const char **b) {
  char **ga;
  const char **gb;
  for (ga = a; *ga; ga++) {
    for (gb = b; *gb; gb++) {
      if (strcmp(*ga, *gb) == 0) {
        return 1;
      }
    }
  }
  return 0;
}

static char *strdup_or_null(const char *s) {
  if (s == NULL) {
    return NULL;
  }
  return strdup(s);
}

static sso_ticket_t sso_ticket_new_with_expiry(const char *user, const char *service,
                                               const char *domain, const char *nonce,
                                               const char **groups, time_t expires) {
  sso_ticket_t t = (sso_ticket_t)malloc(sizeof(struct sso_ticket));
  t->user = strdup_or_null(user);
  t->service = strdup_or_null(service);
  t->domain = strdup_or_null(domain);
  t->nonce = strdup_or_null(nonce);
  t->groups = group_list_dup(groups);
  t->expires = expires;
  return t;
}

sso_ticket_t sso_ticket_new(const char *user, const char *service,
                            const char *domain, const char *nonce,
                            const char **groups, int validity_seconds) {
  time_t expires = time(NULL) + validity_seconds;
  return sso_ticket_new_with_expiry(user, service, domain, nonce, groups, expires);
}

void sso_ticket_free(sso_ticket_t t) {
  if (t->user != NULL) {
    free(t->user);
  }
  if (t->service != NULL) {
    free(t->service);
  }
  if (t->domain != NULL) {
    free(t->domain);
  }
  if (t->nonce != NULL) {
    free(t->nonce);
  }
  if (t->groups != NULL) {
    group_list_free(t->groups);
  }
  free(t);
}

/*
 * Buffered string accumulator, to efficiently concatenate tokens to a
 * fixed-size output buffer.
 */
typedef struct {
  char *buf;
  int bufsz;
  int pos;
} accumulate_buffer;

/*
 * Add a string to the buffer. Return -1 if there is no space
 * left to copy it safely.
 */
static int accum_add(accumulate_buffer *acc, char *s) {
  int n = strlen(s);
  if (acc->pos + n >= acc->bufsz) {
    return -1;
  }
  strcpy(acc->buf + acc->pos, s);
  acc->pos += n;
  return 0;
}

/*
 * Add a string to the buffer, followed by a field separator.
 */
static int accum_add_field(accumulate_buffer *acc, char *s) {
  if (s == NULL) {
    s = "";
  }
  return (accum_add(acc, s) + accum_add(acc, FIELD_SEP_STR));
}

/*
 * Add a time_t to the buffer (serialized as a string), followed by a
 * field separator.
 */
static int accum_add_time(accumulate_buffer *acc, time_t stamp) {
  char int_buf[32];
  sprintf(int_buf, "%d", (int)stamp);
  return (accum_add(acc, int_buf) + accum_add(acc, FIELD_SEP_STR));
}

/*
 * Add a comma-separated list of strings to the output buffer.
 */
static int accum_add_groups(accumulate_buffer *acc, char **groups) {
  int i, r = 0;
  char **g;

  if (groups == NULL) {
    return 0;
  }
  for (i = 0, g = groups; *g; i++, g++) {
    if (i > 0) {
      r += accum_add(acc, GROUP_SEP_STR);
    }
    r += accum_add(acc, *g);
  }
  return r;
}

static char *sso_ticket_serialize(sso_ticket_t t) {
  char buf[1024];
  accumulate_buffer acc = {buf, sizeof(buf), 0};
  int r;

  r = accum_add_field(&acc, SSO_TICKET_VERSION);
  r += accum_add_field(&acc, t->user);
  r += accum_add_field(&acc, t->service);
  r += accum_add_field(&acc, t->domain);
  r += accum_add_field(&acc, t->nonce);
  r += accum_add_time(&acc, t->expires);
  r += accum_add_groups(&acc, t->groups);
  if (r < 0) {
    return NULL;
  }

  return strdup(buf);
}

static char **group_list_parse(char *s) {
  char *token, *ss, *sp = NULL;
  char **groups, **gg;
  int n_groups;

  if (s == NULL || *s == '\0') {
    return NULL;
  }

  for (n_groups = 1, ss = s; *ss; ss++) {
    if (*ss == GROUP_SEP_CH) {
      n_groups++;
    }
  }

  groups = (char **)malloc(sizeof(char *) * (n_groups + 1));

  for (ss = s, gg = groups;; gg++, ss = NULL) {
    token = strtok_r(ss, GROUP_SEP_STR, &sp);
    if (token == NULL) {
      break;
    }
    *gg = strdup(token);
  }
  *gg = NULL;

  return groups;
}

static int sso_ticket_deserialize(sso_ticket_t *t, const char *s, int sz) {
  char *version = NULL,
    *user = NULL,
    *service = NULL,
    *domain = NULL,
    *nonce = NULL;
  char **groups = NULL;
  time_t expires = 0;
  int err = SSO_OK;
  int field_size;
  int i = 0, last = 0, field = 0;
  char *token;

  // Split s into fields, which may be empty. We can't use strtok()
  // because it merges away empty fields, and we need to know about
  // those. The loop extends one further than the end of the input.
  for (; i <= sz; i++) {
    if (i < sz && s[i] != FIELD_SEP_CH) {
      continue;
    }

    field_size = i - last;
    if (field_size > 0) {
      // Make a copy of s[last:i].
      token = (char *)malloc(field_size + 1);
      memcpy(token, s + last, field_size);
      token[field_size] = 0;

      switch (field) {
      case 0:
        version = token;
        break;
      case 1:
        user = token;
        break;
      case 2:
        service = token;
        break;
      case 3:
        domain = token;
        break;
      case 4:
        nonce = token;
        break;
      case 5:
        expires = (time_t)strtol(token, NULL, 10);
        free(token);
        break;
      case 6:
        groups = group_list_parse(token);
        free(token);
        break;
      default:
        err = SSO_ERR_DESERIALIZATION;
        free(token);
        goto fail;
      }
    }
    last = i + 1;
    field++;
  }

  if (version == NULL || strcmp(version, SSO_TICKET_VERSION) != 0) {
    err = SSO_ERR_UNSUPPORTED_VERSION;
    goto fail;
  }

  if (field < 6 || field > 7) {
    err = SSO_ERR_DESERIALIZATION;
    goto fail;
  }

  *t = sso_ticket_new_with_expiry(user, service, domain, nonce, (const char **)groups, expires);

 fail:
  if (version != NULL)
    free(version);
  if (user != NULL)
    free(user);
  if (service != NULL)
    free(service);
  if (domain != NULL)
    free(domain);
  if (nonce != NULL)
    free(nonce);
  if (groups != NULL)
    group_list_free(groups);
  return err;
}

int sso_generate_keys(unsigned char *publicp, unsigned char *secretp) {
  return crypto_sign_keypair(publicp, secretp);
}

static int sso_ticket_validate_fields(sso_ticket_t t) {
  char **gp;

  // Ensure that required fields are set.
  if ((t->user == NULL) || (t->service == NULL) || (t->domain == NULL)) {
    return SSO_ERR_MISSING_REQUIRED_FIELD;
  }

  // Check the syntax of the fields. The only requirement is that they
  // do not contain the separator character, besides that anything
  // will do. The (better) alternative would be to escape the values.
  if ((strchr(t->user, FIELD_SEP_CH) != NULL) ||
      (strchr(t->service, FIELD_SEP_CH) != NULL) ||
      (strchr(t->domain, FIELD_SEP_CH) != NULL)) {
    return SSO_ERR_INVALID_FIELD;
  }
  if (t->groups != NULL) {
    for (gp = t->groups; *gp; gp++) {
      if ((strchr(*gp, FIELD_SEP_CH) != NULL) ||
          strchr(*gp, GROUP_SEP_CH) != NULL) {
        return SSO_ERR_INVALID_FIELD;
      }
    }
  }
  return SSO_OK;
}

int sso_ticket_sign(sso_ticket_t t, const unsigned char *secret_key, char *out,
                    size_t out_size) {
  char *serialized;
  size_t serialized_size, dlen;
  unsigned char *signed_data;
  unsigned long long signed_size;
  int r;

  r = sso_ticket_validate_fields(t);
  if (r != SSO_OK) {
    return r;
  }

  serialized = sso_ticket_serialize(t);
  if (serialized == NULL) {
    return SSO_ERR_SERIALIZATION;
  }
  serialized_size = strlen(serialized);

  signed_size = crypto_sign_BYTES + serialized_size;
  signed_data = (unsigned char *)malloc(signed_size);

  if (crypto_sign(signed_data, &signed_size, (unsigned char *)serialized,
                  (unsigned long long)serialized_size, secret_key) != 0) {
    free(serialized);
    free(signed_data);
    return SSO_ERR_SIGNATURE;
  }

  dlen = out_size;
  r = sso_base64_encode((unsigned char *)out, &dlen, signed_data,
                        (size_t)signed_size);
  free(serialized);
  free(signed_data);
  return r;
}

int sso_ticket_open(sso_ticket_t *t, const char *str,
                    const unsigned char *public_key) {
  size_t encoded_size, signed_size;
  unsigned char *signed_data;
  unsigned char *serialized;
  unsigned long long serialized_size;
  int r;

  encoded_size = signed_size = strlen(str);
  signed_data = (unsigned char *)malloc(encoded_size + 1);
  r = sso_base64_decode(signed_data, &signed_size, (const unsigned char *)str,
                        encoded_size);
  if (r < 0) {
    free(signed_data);
    return r;
  }

  serialized_size = signed_size;
  serialized = (unsigned char *)malloc(serialized_size);

  if (crypto_sign_open(serialized, &serialized_size,
                       (const unsigned char *)signed_data,
                       (unsigned long long)signed_size, public_key) != 0) {
    free(serialized);
    free(signed_data);
    return SSO_ERR_BAD_SIGNATURE;
  }

  r = sso_ticket_deserialize(t, (char *)serialized, serialized_size);
  free(serialized);
  free(signed_data);
  return r;
}

int sso_validate(sso_ticket_t t, const char *service, const char *domain,
                 const char *nonce, const char **groups) {
  if (t->service == NULL || strcmp(t->service, service) != 0) {
    return SSO_ERR_BAD_SERVICE;
  }
  if (t->domain == NULL || strcmp(t->domain, domain) != 0) {
    return SSO_ERR_BAD_DOMAIN;
  }
  if (t->expires < time(NULL)) {
    return SSO_ERR_EXPIRED;
  }
  if (nonce != NULL && (t->nonce == NULL || strcmp(t->nonce, nonce) != 0)) {
    return SSO_ERR_BAD_NONCE;
  }
  if (groups != NULL &&
      (t->groups == NULL || !group_list_overlap(t->groups, groups))) {
    return SSO_ERR_NO_MATCHING_GROUPS;
  }
  return SSO_OK;
}

const char *sso_strerror(int err) {
  switch (err) {
  case SSO_ERR_SERIALIZATION:
    return "serialization failure";
  case SSO_ERR_DESERIALIZATION:
    return "deserialization error (bad format)";
  case SSO_ERR_DECODE64:
    return "deserialization error (base64)";
  case SSO_ERR_SIGNATURE:
    return "signature failure";
  case SSO_ERR_BUFFER_TOO_SMALL:
    return "output buffer is too small";
  case SSO_ERR_BAD_SIGNATURE:
    return "signature verification failure";
  case SSO_ERR_UNSUPPORTED_VERSION:
    return "unsupported ticker version";
  case SSO_ERR_EXPIRED:
    return "ticket expired";
  case SSO_ERR_BAD_SERVICE:
    return "invalid ticket (bad service)";
  case SSO_ERR_BAD_DOMAIN:
    return "invalid ticket (bad domain)";
  case SSO_ERR_NO_MATCHING_GROUPS:
    return "no matching groups";
  case SSO_ERR_BAD_NONCE:
    return "nonce mismatch";
  case SSO_ERR_MISSING_REQUIRED_FIELD:
    return "missing a required field";
  case SSO_ERR_INVALID_FIELD:
    return "a field has invalid syntax";
  default:
    return "unknown error";
  }
}
