#ifndef __sso_base64_h
#define __sso_base64_h 1

#include <sys/types.h>

/* Base64 encoding utilities. */
int sso_base64_encode(unsigned char *dst, size_t *dlen,
                      const unsigned char *src, size_t slen);
int sso_base64_decode(unsigned char *dst, size_t *dlen,
                      const unsigned char *src, size_t slen);
size_t sso_base64_encode_size(size_t slen);

#endif
