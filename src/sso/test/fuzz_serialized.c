// Fuzz the serialized version of a SSO ticket.

#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include "sso.h"

static unsigned char public_key[SSO_PUBLIC_KEY_SIZE], secret_key[SSO_SECRET_KEY_SIZE];
static int public_key_init = 0;
static const unsigned char *get_public_key() {
  if (!public_key_init) {
    sso_generate_keys(public_key, secret_key);
    public_key_init = 1;
  }
  return public_key;
}

int LLVMFuzzerTestOneInput(const uint8_t *data, size_t size) {
  char *buf;
  sso_ticket_t tkt = NULL;
  int r;

  buf = (char *)malloc(size + 1);
  memcpy(buf, data, size);
  buf[size] = 0;

  r = sso_ticket_open(&tkt, (const char *)buf, get_public_key());
  if (r == SSO_OK) {
    sso_ticket_free(tkt);
  }

  free(buf);
  return 0;
}
