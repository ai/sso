#include <string.h>
#include <gtest/gtest.h>

extern "C" {
#include "tweetnacl.h"
}

namespace {

TEST(TweetNaCl, Works) {

  unsigned char pubkey[crypto_sign_PUBLICKEYBYTES];
  unsigned char secret[crypto_sign_SECRETKEYBYTES];
  EXPECT_EQ(0, crypto_sign_keypair(pubkey, secret));

  const char *src_data = "test data";
  unsigned char signed_data[1024];
  unsigned char decoded[1024];
  unsigned long long signed_size = 0;
  unsigned long long decoded_size = 0;

  EXPECT_EQ(0, crypto_sign(signed_data, &signed_size,
                           (unsigned char *)src_data,
                           (unsigned long long)strlen(src_data),
                           secret));
  EXPECT_EQ(0, crypto_sign_open(decoded, &decoded_size,
                                signed_data, signed_size,
                                pubkey));

  EXPECT_EQ(strlen(src_data), decoded_size);
  EXPECT_EQ(0, strncmp((char *)decoded, (char *)src_data, decoded_size));
}

}

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
