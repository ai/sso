#!/bin/sh
#
# Compile libsso with libfuzz, using a specific fuzz entry point.
# Useful libfuzz args include -jobs=N -workers=N for parallelization.
#
# Note: on Debian this probably requires CC=clang-11.
#

dir=$(dirname "$0")
dir=${dir:-.}
top_srcdir=${dir}/..

fuzz_src="$1"
if [ -z "$fuzz_src" ]; then
    echo "Usage: $0 <target_source_file>" >&2
    exit 2
fi
shift

set -e
make -C ${top_srcdir} clean
make -C ${top_srcdir} CC=${CC:-clang} CFLAGS='-g -fsanitize=address,fuzzer-no-link'
${CC:-clang} -I${top_srcdir} -g -fsanitize=address,fuzzer -o fuzz "${fuzz_src}" -L${top_srcdir}/.libs -lsso
LD_LIBRARY_PATH=${top_srcdir}/.libs ./fuzz "$@"
