// Fuzz the contents of a signed, encoded ticket.
// (Encrypts the data coming from the fuzzer).

#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include "base64.h"
#include "sso.h"
#include "tweetnacl.h"

static unsigned char public_key[SSO_PUBLIC_KEY_SIZE],
  secret_key[SSO_SECRET_KEY_SIZE];
static int key_init = 0;

static inline void do_key_init() {
  if (!key_init) {
    sso_generate_keys(public_key, secret_key);
    key_init = 1;
  }
}

static inline const unsigned char *get_public_key() {
  do_key_init();
  return public_key;
}

static inline const unsigned char *get_secret_key() {
  do_key_init();
  return secret_key;
}

static char *static_groups[] = {"g1", "g2", NULL};

int LLVMFuzzerTestOneInput(const uint8_t *data, size_t size) {
  unsigned char *buf;
  unsigned char *b64buf;
  sso_ticket_t tkt = NULL;
  int r;
  size_t b64bufsz;
  unsigned long long signed_size;

  signed_size = crypto_sign_BYTES + size;
  buf = (unsigned char *)malloc(signed_size);
  crypto_sign(buf, &signed_size, data, size, get_secret_key());

  b64bufsz = sso_base64_encode_size(signed_size) * 2 + 1;
  b64buf = (unsigned char *)malloc(b64bufsz);

  r = sso_base64_encode(b64buf, &b64bufsz, buf,
                        (size_t)signed_size);

  r = sso_ticket_open(&tkt, (const char *)b64buf, get_public_key());
  if (r == SSO_OK) {
    sso_validate(tkt, "svc", "domain", NULL, static_groups);
    sso_ticket_free(tkt);
  }

  free(b64buf);
  free(buf);
  return 0;
}
