#include <stdlib.h>
#include <gtest/gtest.h>

#include "sso.h"
extern "C" {
#include "base64.h"
#include "tweetnacl.h"
}

namespace {

static unsigned char alphabet[] = {
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
  20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36,
  37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53,
  54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70,
  71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87,
  88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103,
  104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117,
  118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131,
  132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145,
  146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159,
  160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173,
  174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187,
  188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201,
  202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215,
  216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229,
  230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243,
  244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255
};

static const std::string encoded_alphabet = "AAECAwQFBgcICQoLDA0ODxAREhMUFRYXGBkaGxwdHh8gISIjJCUmJygpKissLS4vMDEyMzQ1Njc4OTo7PD0-P0BBQkNERUZHSElKS0xNTk9QUVJTVFVWV1hZWltcXV5fYGFiY2RlZmdoaWprbG1ub3BxcnN0dXZ3eHl6e3x9fn-AgYKDhIWGh4iJiouMjY6PkJGSk5SVlpeYmZqbnJ2en6ChoqOkpaanqKmqq6ytrq-wsbKztLW2t7i5uru8vb6_wMHCw8TFxsfIycrLzM3Oz9DR0tPU1dbX2Nna29zd3t_g4eLj5OXm5-jp6uvs7e7v8PHy8_T19vf4-fr7_P3-_w==";

static const std::string encoded_alphabet_std = "AAECAwQFBgcICQoLDA0ODxAREhMUFRYXGBkaGxwdHh8gISIjJCUmJygpKissLS4vMDEyMzQ1Njc4OTo7PD0+P0BBQkNERUZHSElKS0xNTk9QUVJTVFVWV1hZWltcXV5fYGFiY2RlZmdoaWprbG1ub3BxcnN0dXZ3eHl6e3x9fn+AgYKDhIWGh4iJiouMjY6PkJGSk5SVlpeYmZqbnJ2en6ChoqOkpaanqKmqq6ytrq+wsbKztLW2t7i5uru8vb6_wMHCw8TFxsfIycrLzM3Oz9DR0tPU1dbX2Nna29zd3t/g4eLj5OXm5+jp6uvs7e7v8PHy8/T19vf4+fr7/P3+/w==";

TEST(Base64, EncodeOk) {
  char out[512] = {0};
  size_t sz = sizeof(out);
  EXPECT_EQ(0, sso_base64_encode((unsigned char *)out, &sz, alphabet, 256));
  EXPECT_EQ(344, int(sso_base64_encode_size(256)));
  EXPECT_EQ(344, int(sz));
  EXPECT_EQ(encoded_alphabet, std::string(out));
}

TEST(Base64, DecodeOk) {
  const char *in = encoded_alphabet.c_str();
  char out[256] = {0};
  size_t sz = sizeof(out);
  EXPECT_EQ(0, sso_base64_decode((unsigned char *)out, &sz, (unsigned char *)in,
                                 (size_t)strlen(in)));
  EXPECT_EQ(256, int(sz));
  EXPECT_EQ(0, memcmp(alphabet, out, sz));
}

TEST(Base64, DecodeStdOk) {
  const char *in = encoded_alphabet_std.c_str();
  char out[256] = {0};
  size_t sz = sizeof(out);
  EXPECT_EQ(0, sso_base64_decode((unsigned char *)out, &sz, (unsigned char *)in,
                                 (size_t)strlen(in)));
  EXPECT_EQ(256, int(sz));
  EXPECT_EQ(0, memcmp(alphabet, out, sz));
}

static const char *unpadded_src[] = {
  "aA",
  "aGU",
  "aGVs",
  "aGVsbA",
  "aGVsbG8",
};

static const char *unpadded_res[] = {
  "h",
  "he",
  "hel",
  "hell",
  "hello",
};

TEST(Base64, DecodeUnpaddedOk) {
  for (int i = 0; i < 5; i++) {
    char out[256] = {0};
    size_t sz = sizeof(out);
    EXPECT_EQ(0, sso_base64_decode((unsigned char *)out, &sz,
                                   (unsigned char *)unpadded_src[i],
                                   (size_t)strlen(unpadded_src[i])));
    EXPECT_EQ(std::string(unpadded_res[i]), std::string(out, sz))
      << "Error decoding '" << unpadded_src[i] << "'";
  }
}
  
class SSO : public testing::Test {
protected:
  virtual void SetUp() { sso_generate_keys(public_key, secret_key); }

  // Return a signed ticket, for test data generation.
  char *sign_ticket(sso_ticket_t t) {
    char buf[1024];
    EXPECT_EQ(0, sso_ticket_sign(t, secret_key, buf, sizeof(buf)));
    return strdup(buf);
  }

  // Return a signed ticket, for test data generation.
  char *sign_and_free_ticket(sso_ticket_t t) {
    char *res = sign_ticket(t);
    // No further use for the original ticket.
    sso_ticket_free(t);
    return res;
  }

  // Sign a ticket with a random secret key.
  char *sign_and_free_ticket_with_random_key(sso_ticket_t t) {
    unsigned char pk[SSO_PUBLIC_KEY_SIZE], sk[SSO_SECRET_KEY_SIZE];
    sso_generate_keys(pk, sk);
    char buf[1024];
    EXPECT_EQ(0, sso_ticket_sign(t, sk, buf, sizeof(buf)));
    // No further use for the original ticket.
    sso_ticket_free(t);
    return strdup(buf);
  }

  // Sign a fake ticket, whose serialization is manually
  // provided in 'contents'.
  char *sign_string(const char *contents) {
    unsigned char buf[1024], encbuf[1024];
    size_t sz = sizeof(buf);
    unsigned long long encsz = sizeof(encbuf);

    crypto_sign(encbuf, &encsz, (const unsigned char *)contents,
                strlen(contents), secret_key);

    sso_base64_encode((unsigned char *)buf, &sz, encbuf, encsz);
    return strdup((const char *)buf);
  }

  unsigned char public_key[SSO_PUBLIC_KEY_SIZE];
  unsigned char secret_key[SSO_SECRET_KEY_SIZE];
};

struct sign_testdata {
  sso_ticket_t tkt;
  int expected_result;
};

TEST_F(SSO, Sign) {
  struct sign_testdata td[] = {
      {sso_ticket_new("user", "service/", "domain", NULL, NULL, 7200), 0},

      {sso_ticket_new(NULL, NULL, NULL, NULL, NULL, 7200),
       SSO_ERR_MISSING_REQUIRED_FIELD},
      {sso_ticket_new(NULL, "service", "domain", NULL, NULL, 7200),
       SSO_ERR_MISSING_REQUIRED_FIELD},
      {sso_ticket_new("user", NULL, "domain", NULL, NULL, 7200),
       SSO_ERR_MISSING_REQUIRED_FIELD},
      {sso_ticket_new("user", "service", NULL, NULL, NULL, 7200),
       SSO_ERR_MISSING_REQUIRED_FIELD},

      {sso_ticket_new("u|ser", "service/", "domain", NULL, NULL, 7200),
       SSO_ERR_INVALID_FIELD},
      {sso_ticket_new("user", "s|ervice/", "domain", NULL, NULL, 7200),
       SSO_ERR_INVALID_FIELD},
      {sso_ticket_new("user", "service/", "d|omain", NULL, NULL, 7200),
       SSO_ERR_INVALID_FIELD},

      {NULL, 0},
  };
  char buf[1024];

  for (struct sign_testdata *tdp = td; tdp->tkt; tdp++) {
    EXPECT_EQ(tdp->expected_result,
              sso_ticket_sign(tdp->tkt, secret_key, buf, sizeof(buf)))
        << "Failed test: #" << (tdp - td);
    sso_ticket_free(tdp->tkt);
  }
}

struct open_testdata {
  char *ticket_str;
  int expected_result;
};

TEST_F(SSO, Open) {
  const char *groups[] = {"users", "wheel", "daemon", NULL};
  struct open_testdata td[] = {
      {sign_and_free_ticket(
           sso_ticket_new("user", "service/", "domain", NULL, NULL, 7200)),
       0},
      {sign_and_free_ticket(
           sso_ticket_new("user", "service/", "domain", NULL, groups, 7200)),
       0},
      {sign_and_free_ticket(sso_ticket_new("user", "", "", NULL, NULL, 7200)), 0},

      {sign_string("5|user|service/|domain|1414402999|"),
       SSO_ERR_UNSUPPORTED_VERSION},
      {sign_string("4|definitely not a ticket"), SSO_ERR_DESERIALIZATION},
      {sign_string("4||||||"), 0},

      {sign_and_free_ticket_with_random_key(
           sso_ticket_new("user", "service/", "domain", NULL, NULL, 7200)),
       SSO_ERR_BAD_SIGNATURE},

      {strdup("not a base64-encoded string"), SSO_ERR_DECODE64},
      {strdup("\377\377\377"), SSO_ERR_DECODE64},
      {strdup("dGVzdHM="), SSO_ERR_BAD_SIGNATURE},
      {strdup(""), SSO_ERR_BAD_SIGNATURE},

      {NULL, 0},
  };

  int i = 0;
  for (struct open_testdata *tdp = td; tdp->ticket_str; i++, tdp++) {
    sso_ticket_t t2 = NULL;
    EXPECT_EQ(tdp->expected_result,
              sso_ticket_open(&t2, tdp->ticket_str, public_key))
      << "Test #" << i << " - Failed decoding ticket: " << tdp->ticket_str;
    if (tdp->expected_result == 0) {
      EXPECT_TRUE(t2);
    } else {
      EXPECT_FALSE(t2);
    }
    if (t2)
      sso_ticket_free(t2);
    free(tdp->ticket_str);
  }
}

static char *big_string() {
  int n = 65535;
  char *big = (char *)malloc(n + 1);
  for (int i = 0; i < n; i++) {
    big[i] = 'a';
  }
  big[n] = 0;
  return big;
}

TEST_F(SSO, InternalSerializationBufferFull) {
  char buf[1024];

  // Ticket with a very big field that should exceed the size of the
  // internal serialization buffer.
  char *big = big_string();
  sso_ticket_t t = sso_ticket_new(big, "service", "domain", NULL, NULL, 7200);
  free(big);
  EXPECT_EQ(SSO_ERR_SERIALIZATION,
            sso_ticket_sign(t, secret_key, buf, sizeof(buf)));
  sso_ticket_free(t);

  // Ticket with a very large list of groups.
  int ng = 255;
  const char *groups[ng + 1];
  for (int i = 0; i < ng; i++)
    groups[i] = "aaaaaaaaaaaaaaaa";
  groups[ng] = NULL;
  t = sso_ticket_new("user", "service/", "domain", NULL, groups, 7200);
  EXPECT_EQ(SSO_ERR_SERIALIZATION,
            sso_ticket_sign(t, secret_key, buf, sizeof(buf)));
  sso_ticket_free(t);
}

TEST_F(SSO, SerializationOutputBufferFull) {
  sso_ticket_t t = sso_ticket_new("user", "service/", "domain", NULL, NULL, 7200);

  // Create a buffer with guard bytes to detect overflow.
  char buf[1024];
  memset(buf, 42, sizeof(buf));

  EXPECT_EQ(SSO_ERR_BUFFER_TOO_SMALL, sso_ticket_sign(t, secret_key, buf, 16));

  for (int i = 16; i < 1024; i++) {
    EXPECT_EQ(42, buf[i]) << "Detected overflow at pos " << i;
  }

  sso_ticket_free(t);
}

struct validate_testcontext {
  const char *service;
  const char *domain;
  const char *nonce;
  const char **ok_groups;
};

struct validate_testdata {
  const char *descr;
  sso_ticket_t t;
  struct validate_testcontext *c;
  int expected_result;
};

TEST_F(SSO, Validation) {
  const char *allowed_groups[] = {"admins", "wheel", NULL};
  const char *groups_ok[] = {"users", "admins", NULL};
  const char *groups_fail[] = {"users", "others", NULL};

  struct validate_testcontext with_groups = {
    "service/", "domain", NULL, allowed_groups,
  };
  struct validate_testcontext without_groups = {
    "service/", "domain", NULL, NULL,
  };
  struct validate_testcontext with_nonce = {
    "service/", "domain", "testnonce", NULL,
  };

  struct validate_testdata td[] = {
    {"ticket_with_groups, validate_groups", 
     sso_ticket_new("user", "service/", "domain", NULL, groups_ok, 7200), &with_groups, 0},
    {"ticket_with_groups, validate_no_groups",
     sso_ticket_new("user", "service/", "domain", NULL, groups_ok, 7200), &without_groups, 0},
    {"ticket_without_groups, validate_groups",
     sso_ticket_new("user", "service/", "domain", NULL, NULL, 7200), &with_groups, SSO_ERR_NO_MATCHING_GROUPS},
    {"ticket_without_groups, validate_no_groups",
     sso_ticket_new("user", "service/", "domain", NULL, NULL, 7200), &without_groups, 0},
    {"ticket_with_bad_groups",
     sso_ticket_new("user", "service/", "domain", NULL, groups_fail, 7200), &with_groups, SSO_ERR_NO_MATCHING_GROUPS},
    {"bad_domain",
     sso_ticket_new("user", "service/", "other", NULL, groups_ok, 7200), &with_groups, SSO_ERR_BAD_DOMAIN},
    {"bad_service",
     sso_ticket_new("user", "other/", "domain", NULL, groups_ok, 7200), &with_groups, SSO_ERR_BAD_SERVICE},
    {"expired",
     sso_ticket_new("user", "service/", "domain", NULL, NULL, -1000), &without_groups, SSO_ERR_EXPIRED},
    {"good_nonce",
     sso_ticket_new("user", "service/", "domain", "testnonce", NULL, 7200), &with_nonce, 0},
    {"bad_nonce",
     sso_ticket_new("user", "service/", "domain", "badnonce", NULL, 7200), &with_nonce, SSO_ERR_BAD_NONCE},
    {"missing_Nonce",
     sso_ticket_new("user", "service/", "domain", NULL, NULL, 7200), &with_nonce, SSO_ERR_BAD_NONCE},
    {NULL, NULL, NULL, 0},
  };

  for (struct validate_testdata *tdp = td; tdp->t; tdp++) {
    EXPECT_EQ(tdp->expected_result,
              sso_validate(tdp->t, tdp->c->service, tdp->c->domain, tdp->c->nonce, tdp->c->ok_groups))
      << "Validation failed: " << tdp->descr;
    sso_ticket_free(tdp->t);
  }
}

static bool is_ticket_equal(sso_ticket_t a, sso_ticket_t b) {
  return (!strcmp(a->user, b->user) &&
          !strcmp(a->service, b->service) &&
          !strcmp(a->domain, b->domain) &&
          !strcmp(a->nonce ?: "NULL", b->nonce ?: "NULL") &&
          a->expires == b->expires);
}

TEST_F(SSO, Serialization) {
  const char *groups_ok[] = {"users", "admins", NULL};
  const char *groups_fail[] = {"users", "others", NULL};

  sso_ticket_t td[] = {
     sso_ticket_new("user", "service/", "domain", NULL, groups_ok, 7200),
     sso_ticket_new("user", "service/", "domain", NULL, NULL, 7200),
     sso_ticket_new("user", "service/", "domain", NULL, groups_fail, 7200),
     sso_ticket_new("user", "service/", "other", NULL, groups_ok, 7200),
     sso_ticket_new("user", "other/", "domain", NULL, groups_ok, 7200),
     sso_ticket_new("user", "service/", "domain", NULL, NULL, -1000),
     sso_ticket_new("user", "service/", "domain", "testnonce", NULL, 7200),
     sso_ticket_new("user", "service/", "domain", "badnonce", NULL, 7200),
     sso_ticket_new("user", "service/", "domain", NULL, NULL, 7200),
     NULL,
  };

  for (sso_ticket_t *tdp = td; *tdp; tdp++) {
    sso_ticket_t cur = *tdp, deserialized = NULL;
    char *serialized;

    serialized = sign_ticket(cur);
    EXPECT_EQ(SSO_OK, sso_ticket_open(&deserialized, serialized, public_key));
    EXPECT_NE(cur, deserialized);
    EXPECT_EQ(true, is_ticket_equal(cur, deserialized));
    free(serialized);
  }
}

} // namespace

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
