/* Copyright (c) 2012 Autistici/Inventati <info@autistici.org>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <time.h>
#include <unistd.h>

#include "sso.h"

#define CHECK_OK(x)                                                            \
  {                                                                            \
    int _err = (x);                                                            \
    if (_err != SSO_OK) {                                                      \
      fprintf(stderr, "SSO Error: %s\n", sso_strerror(_err));                  \
      exit(1);                                                                 \
    }                                                                          \
  }

static void write_to_file(const char *path, unsigned char *contents,
                          size_t sz) {
  FILE *fp = fopen(path, "w");
  if (fp == NULL) {
    return;
  }
  fwrite(contents, 1, sz, fp);
  fclose(fp);
}

static size_t read_from_file(const char *path, unsigned char **out) {
  size_t sz;
  FILE *fp = fopen(path, "r");
  if (fp == NULL) {
    return -1;
  }
  fseek(fp, 0, SEEK_END);
  sz = ftell(fp);
  fseek(fp, 0, SEEK_SET);

  *out = (unsigned char *)malloc(sz + 1);
  if (0 >= fread(*out, 1, sz, fp)) {
    fclose(fp);
    return -1;
  }
  (*out)[sz] = 0;
  fclose(fp);
  return sz;
}

void ssotool_gen_keys(const char *public_key_file,
                      const char *secret_key_file) {
  unsigned char public_key[SSO_PUBLIC_KEY_SIZE];
  unsigned char secret_key[SSO_SECRET_KEY_SIZE];
  CHECK_OK(sso_generate_keys(public_key, secret_key));
  write_to_file(public_key_file, public_key, sizeof(public_key));
  printf("public key written to %s\n", public_key_file);
  write_to_file(secret_key_file, secret_key, sizeof(secret_key));
  printf("secret key written to %s\n", secret_key_file);
}

void ssotool_sign(const char *secret_key_file, const char *user,
                  const char *service, const char *domain,
                  const char *nonce) {
  unsigned char *secret_key = NULL;
  char out[1024];
  sso_ticket_t tkt;
  size_t sz;

  sz = read_from_file(secret_key_file, &secret_key);
  if (sz < 0) {
    fprintf(stderr, "Error: could not read secret key\n");
    exit(2);
  }

  tkt = sso_ticket_new(user, service, domain, nonce, NULL, 9600);
  CHECK_OK(sso_ticket_sign(tkt, secret_key, out, sizeof(out) - 1));
  printf("%s\n", out);
}

void ssotool_verify(const char *public_key_file, const char *service,
                    const char *domain, const char *nonce,
                    const char *ticket) {
  unsigned char *public_key = NULL;
  sso_ticket_t t = NULL;
  size_t sz;

  sz = read_from_file(public_key_file, &public_key);
  if (sz < 0) {
    fprintf(stderr, "Error: could not read public key\n");
    exit(2);
  }

  CHECK_OK(sso_ticket_open(&t, ticket, public_key));
  CHECK_OK(sso_validate(t, service, domain, nonce, NULL));
  printf("ok\n");
}

void show_help() {
  fprintf(stderr, "Usage: ssotool {--sign|--gen-keys} [<options>...]\n"
                  "Options:\n"
                  "  --help             show this help message\n"
                  "  --gen-keys, -k     generate a new public/secret keypair\n"
                  "  --sign, -s         create and sign a new ticket\n"
                  "  --verify, -v       verify a ticket\n"
                  "\n"
                  "Options for --gen-keys:\n"
                  "\n"
                  "  --public-key FILE  write the public key to FILE\n"
                  "  --secret-key FILE  write the secret key to FILE\n"
                  "\n"
                  "Options for --sign:\n"
                  "\n"
                  "  --user USER        username\n"
                  "  --service SERVICE  service name\n"
                  "  --domain DOMAIN    SSO domain\n"
                  "  --nonce NONCE      unique nonce\n"
                  "  --secret-key FILE  read the secret key from FILE\n"
                  "\n");
}

void die(const char *msg) {
  fprintf(stderr, "%s\n", msg);
  exit(1);
}

void die_and_help(const char *msg) {
  fprintf(stderr, "%s\n", msg);
  fprintf(stderr, "\n");
  show_help();
  exit(1);
}

int main(int argc, char **argv) {
  int do_sign = 0;
  int do_verify = 0;
  int do_gen_keys = 0;
  const char *public_key_file = "public.key";
  const char *secret_key_file = "secret.key";
  const char *user = NULL;
  const char *service = NULL;
  const char *domain = NULL;
  const char *nonce = NULL;

  while (1) {
    int c, option_index = 0;
    static struct option long_options[] = {
        {"help", 0, 0, 'h'},
        {"sign", 0, 0, 's'},
        {"verify", 0, 0, 'v'},
        {"gen-keys", 0, 0, 'k'},
        {"public-key", 1, 0, 'P'},
        {"secret-key", 1, 0, 'S'},
        {"user", 1, 0, 'u'},
        {"service", 1, 0, 'z'},
        {"domain", 1, 0, 'd'},
        {"nonce", 1, 0, 'n'},
        {NULL, 0, 0, 0},
    };

    c = getopt_long(argc, argv, "hskP:S:", long_options, &option_index);
    if (c == -1)
      break;

    switch (c) {
    case 'h':
      show_help();
      return 0;
    case 's':
      do_sign = 1;
      break;
    case 'v':
      do_verify = 1;
      break;
    case 'k':
      do_gen_keys = 1;
      break;
    case 'P':
      public_key_file = optarg;
      break;
    case 'S':
      secret_key_file = optarg;
      break;
    case 'u':
      user = optarg;
      break;
    case 'z':
      service = optarg;
      break;
    case 'd':
      domain = optarg;
      break;
    case 'n':
      nonce = optarg;
      break;
    default:
      die_and_help("Error parsing options");
    }
  }

  if (((int)do_sign + (int)do_gen_keys + (int)do_verify) != 1) {
    die("Specify one of --sign, --verify or --gen-keys!");
  }

  if (do_verify) {
    if (!public_key_file) {
      die("Specify the location of the public key with --public-key");
    }
    if (!service || !domain) {
      die("Both --service and --domain must be specified");
    }
    if (argc - optind != 1) {
      die("One argument is required");
    }
    ssotool_verify(public_key_file, service, domain, nonce, argv[optind]);
  } else if (do_sign) {
    if (argc != optind) {
      die("Too many arguments.");
    }
    if (!secret_key_file) {
      die("Specify the location of the secret key with --secret-key");
    }
    if (!user || !service || !domain) {
      die("All of --user, --service and --domain must be specified");
    }
    ssotool_sign(secret_key_file, user, service, domain, nonce);
  } else if (do_gen_keys) {
    ssotool_gen_keys(public_key_file, secret_key_file);
  }

  return 0;
}
