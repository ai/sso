#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

static int fd = -1;

/*
 * Read cryptographically strong random bytes. Any errors will cause
 * the program to abort (there is no meaningful recovery from lack of
 * randomness).
 */
void randombytes(unsigned char *out, unsigned long long n) {
  int r;

  if (fd == -1) {
    fd = open("/dev/urandom", O_RDONLY);
    if (fd < 0) {
      abort();
    }
  }

  while (n > 0) {
    r = read(fd, out, n);
    if (r < 1) {
      abort();
    }
    n -= r;
    out += r;
  }
}
