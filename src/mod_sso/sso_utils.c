/* Copyright (c) 2008 Autistici/Inventati <info@autistici.org>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include <ctype.h>

#include "ap_config.h"
#include "apr_lib.h"
#include "apr_escape.h"
#include "apr_strings.h"
#include "httpd.h"

#include "http_config.h"
#include "http_core.h"
#include "http_log.h"
#include "http_protocol.h"
#include "http_main.h"

#include "mod_sso.h"

// Ugly implementation of url decoding. Returns a newly allocated
// string (from the pool). We're using this because
// apr_punescape_url() segfaults on certain invalid inputs.
const char *modsso_url_decode(apr_pool_t *p, const char *i) {
  char *o, *output = (char *)apr_palloc(p, 1 + strlen(i));
  char state = 'N';
  char buf[3], obuf[2];
  for (o = output; *i; i++) {
    switch(state) {
    case 'N':
      if (*i == '%') state = 1;
      else *o++ = *i;
      break;
    case 1:
       {
         char c = tolower(*i);
         buf[0] = *i;
         if ((c >= 'a' && c <= 'f') || (c >= '0' && c <= '9')) {
           state = 2;
         } else {
           *o++ = '%';
           *o++ = *i;
           state = 'N';
         }
         break;
       }
    case 2:
       {
         char c = tolower(*i);
         if ((c >= 'a' && c <= 'f') || (c >= '0' && c <= '9')) {
           buf[1] = *i;
           buf[2] = 0;
           obuf[0] = strtol(buf, 0, 16);
           obuf[1] = 0;
           if (obuf[0])
              *o++ = obuf[0];
         } else {
           *o++ = '%';
           *o++ = buf[0];
           *o++ = *i;
         }
         state = 'N';
         break;
       }
    }
  }
  if (state != 'N') {
    *o++ = buf[0];
    if (state > 1)
       *o++ = buf[1];
  }
  *o = '\0';
  return output;
}

const char *modsso_url_encode(apr_pool_t *p, const char *s) {
  return apr_pescape_urlencoded(p, s);
}

int modsso_parse_query_string(apr_pool_t *p, const char *str, modsso_params_t params) {
  char *tmp = apr_pstrdup(p, str), *strptr = NULL;

  params->d = NULL;
  params->t = NULL;

  if (str == NULL) {
    return 0;
  }

  while (1) {
    const char *token;
    char *value, *sep;
    token = strtok_r(tmp, "&", &strptr);
    if (token == NULL) {
      break;
    }
    tmp = NULL;

    token = modsso_url_decode(p, token);

    sep = strchr(token, '=');
    if (sep == NULL) {
      continue;
    }
    *sep++ = '\0';
    value = sep;

    if (!strcmp(token, "d")) {
      params->d = value;
    } else if (!strcmp(token, "t")) {
      params->t = value;
    }
  }

  if (!params->d || !params->d[0]
      || !params->t || !params->t[0]) {
    return -1;
  }

  return 0;
}

struct modsso_cookie_do {
  request_rec *r;
  const char *name;
  char *value;
};

/* Iterate through the cookies, isolate our cookie and then remove it.
 *
 * If our cookie appears two or more times, but with different values,
 * remove it twice and set the duplicated flag to true. Remove any
 * $path or other attributes following our cookie if present. If we end
 * up with an empty cookie, remove the whole header.
 */
static int extract_cookie_line(void *varg, const char *key, const char *val)
{
  struct modsso_cookie_do *v = varg;
  char *last1, *last2;
  char *cookie = apr_pstrdup(v->r->pool, val);
  const char *name = apr_pstrcat(v->r->pool, v->name, "=", NULL);
  apr_size_t len = strlen(name);
  const char *comma = ",";
  char *next1;
  const char *semi = ";";
  char *next2;

  /* find the cookie called name */
  next1 = apr_strtok(cookie, comma, &last1);
  while (next1) {
    next2 = apr_strtok(next1, semi, &last2);
    while (next2) {
      char *trim = next2;
      while (apr_isspace(*trim)) {
        trim++;
      }
      if (!strncmp(trim, name, len)) {
        v->value = apr_pstrdup(v->r->pool, trim + len);
      }
      next2 = apr_strtok(NULL, semi, &last2);
    }

    next1 = apr_strtok(NULL, comma, &last1);
  }

  return 1;
}

/**
 * Read the value of a cookie.
 *
 * Almost a copy of util_cookies.c:ap_cookie_read, but we can't link
 * to that one in testing, so we reproduce most of its functionality.
 *
 * @param r Pointer to the request_rec structure.
 * @param cookie_name Name of the cookie to retrieve.
 * @return the cookie value, or an empty string if no cookie was found.
 */
char *modsso_get_cookie(request_rec *r, const char *cookie_name) {
  struct modsso_cookie_do v;

  v.r = r;
  v.value = NULL;
  v.name = cookie_name;
  apr_table_do(extract_cookie_line, &v, r->headers_in,
               "Cookie", "Cookie2", NULL);
  return v.value;
}

/**
 * Set a cookie.
 *
 * Almost a copy of util_cookies.c:ap_cookie_write, but we can't link
 * to that one in testing, so we reproduce most of its functionality.
 */
void modsso_set_cookie(request_rec *r, const char *cookie_name, 
                       const char *value, const char *path)
{
  const char *rfc2109;

  rfc2109 = apr_pstrcat(r->pool, cookie_name, "=", value, ";Path=", path, ";HttpOnly;Secure;Version=1", NULL);
  apr_table_addn(r->err_headers_out, "Set-Cookie", rfc2109);
}

void modsso_del_cookie(request_rec *r, const char *cookie_name, const char *path)
{
  const char *rfc2109;

  rfc2109 = apr_pstrcat(r->pool, cookie_name, "=;Path=", path, ";Version=1;Expires=Thu, 01 Jan 1970 00:00:00 GMT", NULL);
  apr_table_addn(r->err_headers_out, "Set-Cookie", rfc2109);
}

int modsso_read_fixed_size_file(apr_pool_t *pool, const char *path, size_t size,
                                const unsigned char **out) {
  char *m = NULL;
  int status;
  apr_file_t *file;
  apr_size_t n;

  if (apr_file_open(&file, path, APR_FOPEN_READ, 0, pool) != APR_SUCCESS)
    goto fail;

  n = size;
  m = apr_palloc(pool, n);
  status = apr_file_read(file, m, &n);
  apr_file_close(file);
  if (status != APR_SUCCESS || n != size)
    goto fail;

  *out = (unsigned char *)m;
  return 0;

fail:
  // apr_pfree(pool, m);
  return -1;
}
