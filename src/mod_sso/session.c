/* Copyright (c) 2017 Autistici/Inventati <info@autistici.org>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include <openssl/md5.h>
#include <openssl/hmac.h>

#include "httpd.h"
#include "http_config.h"
#include "http_request.h"
#include "ap_config.h"
#include "apr_strings.h"
#include "apr_file_io.h"

#include "base64.h"
#include "mod_sso.h"

static const char *session_cookie_name = "_sso_local_session";

/**
 * Generate a temporary key (bad!).
 */
int modsso_session_generate_temp_key(apr_pool_t *pool, unsigned char *out,
                                     size_t *outsz) {
  if (*outsz < MODSSO_SESSION_KEY_SIZE)
    return -1;
  *outsz = MODSSO_SESSION_KEY_SIZE;
  apr_generate_random_bytes(out, *outsz);
  return 0;
}

/**
 * Save a unique token in a hmac-protected cookie.
 */
static int compute_hmac(const char *value, size_t value_len,
                        const unsigned char *key, size_t key_len, char *output,
                        size_t *outsz) {
  unsigned int output_len;
  if (HMAC(EVP_sha1(), (const void *)key, key_len, (const unsigned char *)value,
           value_len, (unsigned char *)output, &output_len) == NULL) {
    return -1;
  }
  *outsz = output_len;
  return 0;
}

/**
 * Deserialize a session and output the value contained in it. The
 * 'value' argument will be allocated on the pool by the function.
 */
int modsso_session_deserialize(apr_pool_t *pool, const unsigned char *key,
                               size_t key_len, const char *serialized_value,
                               char **value, size_t *value_len) {
  char *p, *enc_value, *enc_signature;
  char *signature, *verify_sig;
  size_t signature_len, verify_len;
  int i;

  p = strchr(serialized_value, '.');
  if (p == NULL) {
    return -1;
  }
  i = p - serialized_value;
  enc_value = apr_pstrndup(pool, serialized_value, i);
  enc_signature = apr_pstrdup(pool, serialized_value + (i + 1));

  // The two tokens are in outbuf (value) and p (signature).  Let's
  // base64-decode them.
  *value_len = i;
  *value = apr_palloc(pool, i + 1);
  if (sso_base64_decode((unsigned char *)(*value), value_len,
                        (const unsigned char *)enc_value, i) != 0) {
    return -1;
  }
  // Zero-terminate value, as we'll be using it as a string later.
  (*value)[*value_len] = '\0';

  signature_len = strlen(enc_signature);
  signature = apr_palloc(pool, signature_len + 1);
  if (sso_base64_decode((unsigned char *)signature, &signature_len,
                        (const unsigned char *)enc_signature,
                        signature_len) != 0) {
    return -1;
  }

  // Compute the signature again and compare.
  verify_len = EVP_MAX_MD_SIZE;
  verify_sig = apr_palloc(pool, verify_len);
  if (compute_hmac(*value, *value_len, key, key_len, verify_sig, &verify_len) <
      0) {
    return -1;
  }
  if (verify_len != signature_len ||
      (memcmp(verify_sig, signature, signature_len) != 0)) {
    return -1;
  }
  return 0;
}

/**
 * Sign and serialize a value. The 'output' argument will be allocated
 * by the function.
 */
int modsso_session_serialize(apr_pool_t *pool, const unsigned char *key,
                             size_t key_len, const char *value,
                             size_t value_len, char **output) {
  size_t signature_len, enc_value_len, enc_signature_len;
  char *signature, *enc_value, *enc_signature, *tmp;

  // Compute signature.
  signature_len = EVP_MAX_MD_SIZE;
  signature = apr_palloc(pool, signature_len);
  if (compute_hmac(value, value_len, key, key_len, signature, &signature_len) <
      0) {
    return -1;
  }

  // Base64 encode both terms to NUL-terminated strings.
  enc_value_len = sso_base64_encode_size(value_len) + 1;
  enc_value = apr_palloc(pool, enc_value_len);
  if (sso_base64_encode((unsigned char *)enc_value, &enc_value_len,
                        (const unsigned char *)value, value_len) < 0) {
    return -2;
  }
  enc_signature_len = sso_base64_encode_size(signature_len) + 1;
  enc_signature = apr_palloc(pool, enc_signature_len);
  if (sso_base64_encode((unsigned char *)enc_signature, &enc_signature_len,
                        (const unsigned char *)signature, signature_len) < 0) {
    return -3;
  }

  // Concatenate with dot and NUL-terminate.
  tmp = apr_palloc(pool, enc_value_len + enc_signature_len + 2);
  memcpy(tmp, enc_value, enc_value_len);
  tmp[enc_value_len] = '.';
  memcpy(tmp + (enc_value_len + 1), enc_signature, enc_signature_len);
  tmp[enc_value_len + enc_signature_len + 1] = '\0';
  *output = tmp;

  return 0;
}

int modsso_session_read(request_rec *r, const unsigned char *key,
                        size_t key_len, char **value,
                        const char *cookie_path) {
  const char *serialized;
  size_t value_len;
  int ret = -1;

  serialized = modsso_get_cookie(r, session_cookie_name);
  if (serialized == NULL) {
    // No session cookie set, return immediately.
    return -1;
  }

  if (modsso_session_deserialize(r->pool, key, key_len, serialized, value,
                                 &value_len) < 0) {
    goto fail;
  }

  ret = 0;

 fail:
  modsso_del_cookie(r, session_cookie_name, cookie_path);
  return ret;
}

int modsso_session_save(request_rec *r, const unsigned char *key,
                        size_t key_len, const char *unique_id,
                        const char *path) {
  char *serialized;

  if (modsso_session_serialize(r->pool, key, key_len, unique_id,
                               strlen(unique_id), &serialized) < 0) {
    return -1;
  }

  modsso_set_cookie(r, session_cookie_name, serialized, path);
  return 0;
}
