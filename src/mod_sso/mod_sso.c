/* Copyright (c) 2012 Autistici/Inventati <info@autistici.org>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>

#include "ap_config.h"
#include "apr_strings.h"
#include "httpd.h"

#include "http_config.h"
#include "http_core.h"
#include "http_log.h"
#include "http_main.h"
#include "http_protocol.h"
#include "http_request.h"

#include "mod_sso.h"

#if MODULE_MAGIC_NUMBER_MAJOR < 20100714
#error "Requires Apache >= 2.4"
#endif

#define SSO_REQUIRE_NAME "sso"
#include "mod_auth.h"

static const char *sso_auth_type = "SSO";

static inline int is_sso_auth(request_rec *r) {
  const char *type = ap_auth_type(r);
  return (type && apr_strnatcasecmp(type, sso_auth_type) == 0);
}

extern module AP_MODULE_DECLARE_DATA sso_module;

typedef struct {
  const char *login_server;
  const char *login_server_origin;
  const char *domain;
  const char *service;

  // Note: public_key is a binary buffer (non zero-terminated).
  const unsigned char *public_key;

  // Same for the session_key.
  const unsigned char *session_key;

  // All known groups (2.4: unused).
  apr_array_header_t *groups;
} modsso_config;

typedef const char *(*CMD_HAND_TYPE)();

static char *groups_array_to_commasep_string(apr_pool_t *p,
                                             apr_array_header_t *groups) {
  return apr_array_pstrcat(p, groups, ',');
}

static char *groups_charp_to_string(apr_pool_t *p, const char **groups) {
  apr_array_header_t *arr = apr_array_make(p, 1, sizeof(char *));
  const char **gptr;
  for (gptr = groups; *gptr; gptr++) {
    *(const char **)apr_array_push(arr) = *gptr;
  }
  return groups_array_to_commasep_string(p, arr);
}

/**
 * Create a modsso_config structure.
 *
 * @param p APR allocation pool.
 * @param s unused.
 * @return a newly allocated modsso_config with default values.
 */
static void *create_modsso_config(apr_pool_t *p, char *s) {
  // This module's configuration structure.
  modsso_config *newcfg;

  // Allocate memory from the provided pool.
  newcfg = (modsso_config *)apr_palloc(p, sizeof(modsso_config));

  // Set default values.
  newcfg->login_server = NULL;
  newcfg->login_server_origin = NULL;
  newcfg->service = NULL;
  newcfg->domain = NULL;
  newcfg->public_key = NULL;
  newcfg->session_key = NULL;
  newcfg->groups = NULL;

  // Return the created configuration struct.
  return (void *)newcfg;
}

static void *merge_modsso_config(apr_pool_t *p, void *base, void *add) {
  modsso_config *cbase = (modsso_config *)base;
  modsso_config *cadd = (modsso_config *)add;
  modsso_config *newcfg = (modsso_config *)apr_palloc(p, sizeof(modsso_config));

  newcfg->login_server =
      cadd->login_server ? cadd->login_server : cbase->login_server;
  newcfg->login_server_origin =
      cadd->login_server_origin ? cadd->login_server_origin : cbase->login_server_origin;
  newcfg->service = cadd->service ? cadd->service : cbase->service;
  newcfg->domain = cadd->domain ? cadd->domain : cbase->domain;
  newcfg->public_key = cadd->public_key ? cadd->public_key : cbase->public_key;
  newcfg->session_key = cadd->session_key ? cadd->session_key : cbase->session_key;

  // Groups are not merged, last takes precedence (if set).
  newcfg->groups = cadd->groups ? cadd->groups : cbase->groups;

  return (void *)newcfg;
}

static const char *set_modsso_login_server(cmd_parms *parms, void *mconfig,
                                           const char *arg) {
  modsso_config *s_cfg = (modsso_config *)mconfig;
  char *origin, *p;

  // Ignore an eventual https:// prefix.
  if (!strncmp(arg, "https://", 8)) {
    arg += 8;
  }
  s_cfg->login_server = arg;

  // The CORS Origin for the login server is obtained by stripping any
  // path component from the URL.
  origin = apr_pstrdup(parms->pool, arg);
  if ((p = strchr(origin, '/')) != NULL) {
    *p = '\0';
  }
  s_cfg->login_server_origin = apr_pstrcat(parms->pool, "https://", origin, NULL);

  return NULL;
}

static const char *set_modsso_service(cmd_parms *parms, void *mconfig,
                                      const char *arg) {
  modsso_config *s_cfg = (modsso_config *)mconfig;
  s_cfg->service = arg;
  return NULL;
}

static const char *set_modsso_domain(cmd_parms *parms, void *mconfig,
                                     const char *arg) {
  modsso_config *s_cfg = (modsso_config *)mconfig;
  s_cfg->domain = arg;
  return NULL;
}

static const char *set_modsso_public_key_file(cmd_parms *parms, void *mconfig,
                                              const char *arg) {
  modsso_config *s_cfg = (modsso_config *)mconfig;

  if (modsso_read_fixed_size_file(parms->pool, arg, SSO_PUBLIC_KEY_SIZE, &s_cfg->public_key) < 0) {
    return "Could not read SSOPublicKeyFile";
  }
  return NULL;
}

static const char *set_modsso_session_key_file(cmd_parms *parms, void *mconfig,
                                               const char *arg) {
  modsso_config *s_cfg = (modsso_config *)mconfig;

  if (modsso_read_fixed_size_file(parms->pool, arg, MODSSO_SESSION_KEY_SIZE, &s_cfg->session_key) < 0) {
    return "Could not read SSOSessionKeyFile";
  }
  return NULL;
}

static const command_rec mod_sso_cmds[] = {
    AP_INIT_TAKE1("SSOLoginServer", (CMD_HAND_TYPE)set_modsso_login_server,
                  NULL, OR_ALL,
                  "SSOLoginServer (string) URL of the login server."),
    AP_INIT_TAKE1("SSOService", (CMD_HAND_TYPE)set_modsso_service, NULL, OR_ALL,
                  "SSOService (string) SSO Service"),
    AP_INIT_TAKE1("SSODomain", (CMD_HAND_TYPE)set_modsso_domain, NULL, OR_ALL,
                  "SSODomain (string) SSO Domain"),
    AP_INIT_TAKE1(
        "SSOPublicKeyFile", (CMD_HAND_TYPE)set_modsso_public_key_file, NULL,
        RSRC_CONF,
        "SSOPublicKeyFile (string) Location of the login server public key"),
    AP_INIT_TAKE1(
        "SSOSessionKeyFile", (CMD_HAND_TYPE)set_modsso_session_key_file, NULL,
        RSRC_CONF,
        "SSOSessionKeyFile (string) Location of the local session secret key"),
    {NULL}};

/**
 * Send the given text to the client.
 *
 * Creates a response with status 200 OK and a Content-Type of text/html.
 *
 * @param r Pointer to the request_rec structure.
 * @param s Body of the response.
 */
static int http_sendstring(request_rec *r, const char *s) {
  conn_rec *c = r->connection;
  apr_bucket *b;
  apr_bucket_brigade *bb = apr_brigade_create(r->pool, c->bucket_alloc);
  b = apr_bucket_transient_create(s, strlen(s), c->bucket_alloc);
  APR_BRIGADE_INSERT_TAIL(bb, b);
  b = apr_bucket_eos_create(c->bucket_alloc);
  APR_BRIGADE_INSERT_TAIL(bb, b);

  ap_set_content_type(r, "text/html");
  if (ap_pass_brigade(r->output_filters, bb) != APR_SUCCESS)
    return HTTP_INTERNAL_SERVER_ERROR;
  return OK;
}

/**
 * Return a redirect response to the client.
 *
 * @param r Pointer to the request_rec structure.
 * @param location URL to redirect to.
 */
static int http_redirect(request_rec *r, const char *location) {
  apr_table_set(r->headers_out, "Location", location);
  apr_table_setn(r->headers_out, "Cache-Control", "no-cache");
  return HTTP_MOVED_TEMPORARILY;
}

/**
 * Get the full URI of the request_rec's request location.
 *
 * NOTE: the protocol is always forced to https.
 *
 * @param r Pointer to the request_rec structure.
 * @param service_host Host part of the SSO service.
 */
static char *full_uri(request_rec *r, const char *service_host) {
  char *result = apr_pstrcat(r->pool, "https://", service_host, r->uri, NULL);
  if (r->args) {
    result = apr_pstrcat(r->pool, result, "?", r->args, NULL);
  }
  return result;
}

/**
 * Check if a redirection url is valid for our service.
 *
 * Also check for presence of some forbidden characters.
 *
 * @param redir Destination URL of the redirection.
 * @param service SSO service.
 * @return true if the redirection is valid.
 */
static int is_valid_redir(request_rec *r, const char *redir,
                          const char *service) {
  const char *c;
  char *prefix = apr_pstrcat(r->pool, "https://", service, NULL);
  if (strlen(redir) < strlen(prefix)) {
    return 0;
  }
  if (strncmp(redir, prefix, strlen(prefix)) != 0) {
    return 0;
  }
  for (c = redir; *c; c++) {
    if (*c < 32 || *c == ';')
      return 0;
  }
  return 1;
}

static char *get_cookie_name(request_rec *r) {
  char *cookie_name;
  const char *auth_name = ap_auth_name(r);
  if (auth_name) {
    cookie_name = apr_pstrcat(r->pool, "SSO_", auth_name, NULL);
  } else {
    cookie_name = "SSO";
  }
  return cookie_name;
}

// Parse the service spec
static int parse_service(request_rec *r, modsso_config *s_cfg,
                         const char **service, const char **service_host,
                         const char **service_path) {
  char *svc;

  // Make sure the service ends with a slash.
  if (s_cfg->service != NULL &&
      s_cfg->service[strlen(s_cfg->service) - 1] != '/') {
    svc = apr_pstrcat(r->pool, s_cfg->service, "/", NULL);
  } else {
    svc = apr_pstrdup(r->pool, s_cfg->service);
  }

  *service = apr_pstrdup(r->pool, svc);

  // If a service name is configured (including the host), we
  // just split it on the first slash.
  if (svc[0] != '/') {
    char *slash = strchr(svc, '/');
    if (slash) {
      *slash++ = '\0';
    }
    *service_host = apr_pstrdup(r->pool, svc);
    *service_path = apr_pstrcat(r->pool, "/", slash, NULL);
    return 0;
  }

  // A relative or empty service name means we have to autodetect
  // the host (and port) part.
  *service_path = apr_pstrdup(r->pool, svc);

  const char *host_hdr = apr_table_get(r->headers_in, "Host");
  if (!host_hdr) {
    if (!r->server->defn_name) {
      *service = "error";
      ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server,
                   "sso: SSOService could not be determined");
      return -1;
    }
    *service_host = r->server->defn_name;
    int port = ap_get_server_port(r);
    if (port > 0 && port != 80 && port != 443) {
      *service_host = apr_pstrcat(r->pool, *service_host, ":",
                                  apr_ltoa(r->pool, port), NULL);
    }
  } else {
    *service_host = host_hdr;
  }
  *service = apr_pstrcat(r->pool, *service_host, *service_path, NULL);
  ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, r->server,
               "sso: SSOService automatically determined: %s", *service);
  return 0;
}

static int check_config(request_rec *r, modsso_config *s_cfg) {
  if (!s_cfg->login_server) {
    ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server,
                 "sso: SSOLoginServer is not defined!");
    return 0;
  }
  if (!s_cfg->service) {
    ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server,
                 "sso: SSOService is not defined!");
    return 0;
  }
  if (!s_cfg->domain) {
    ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server,
                 "sso: SSODomain is not defined!");
    return 0;
  }
  if (!s_cfg->public_key) {
    ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server,
                 "sso: SSOPublicKeyFile is not defined!");
    return 0;
  }
  return 1;
}

/**
 * Apache method handler for mod_sso.
 *
 * @param r Pointer to the request_rec structure.
 */
static int mod_sso_method_handler(request_rec *r) {
  const char *uri, *sso_cookie_name;
  const char *service = NULL, *service_host = NULL, *service_path = NULL;
  char *sso_logout_path, *sso_login_path;

  // Get the module configuration
  modsso_config *s_cfg =
      (modsso_config *)ap_get_module_config(r->per_dir_config, &sso_module);
  uri = r->uri;

  // Return immediately if there's nothing to do (check the AuthType)
  if (!is_sso_auth(r)) {
    ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, r->server,
                 "sso: invalid authentication type");
    return DECLINED;
  }

  sso_cookie_name = get_cookie_name(r);

  ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, r->server, "sso: cookie_name \"%s\"",
               sso_cookie_name);

  // Check if the required parameters are defined.
  if (!check_config(r, s_cfg)) {
    return HTTP_INTERNAL_SERVER_ERROR;
  }

  // Parse the service into host/path (guess it if not specified).
  if (parse_service(r, s_cfg, &service, &service_host, &service_path) != 0) {
    ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server,
                 "sso: could not parse service \"%s\"", s_cfg->service);
    return HTTP_BAD_REQUEST;
  }

  // Handle /sso_logout
  sso_logout_path = apr_pstrcat(r->pool, service_path, "sso_logout", NULL);
  ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, r->server,
               "sso: logout? \"%s\" \"%s\"", sso_logout_path, uri);
  if (!strcmp(uri, sso_logout_path)) {
    modsso_del_cookie(r, sso_cookie_name, service_path);
    apr_table_setn(r->err_headers_out, "Access-Control-Allow-Origin", s_cfg->login_server_origin);
    apr_table_setn(r->err_headers_out, "Access-Control-Allow-Credentials", "true");
    apr_table_setn(r->err_headers_out, "Cache-Control", "no-cache");
    return http_sendstring(r, "OK");
  }

  // Handle /sso_login
  sso_login_path = apr_pstrcat(r->pool, service_path, "sso_login", NULL);
  ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, r->server,
               "sso: login? \"%s\" \"%s\"", sso_login_path, uri);
  if (!strcmp(uri, sso_login_path)) {
    struct modsso_params params;
    sso_ticket_t t;
    const char *redir;
    char *unique_id = NULL;
    int err;

    if (!r->args) {
      ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server,
                   "sso: invalid sso_login request (no query args)");
      return HTTP_BAD_REQUEST;
    }

    // Parse query params
    if (modsso_parse_query_string(r->pool, r->args, &params) < 0) {
      ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server,
                   "sso: invalid parameters for sso_login: %s", r->args);
      return HTTP_BAD_REQUEST;
    }

    // Check that the redirect destination is valid.
    redir = modsso_url_decode(r->pool, params.d);
    if (!is_valid_redir(r, redir, service)) {
      ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server,
                   "sso: invalid redirect to %s", redir);
      return HTTP_BAD_REQUEST;
    }

    // Parse the SSO ticket and validate the nonce with the session.
    // Only do this if a session key is set (sessions are enabled).
    if (s_cfg->session_key != NULL) {
      if (modsso_session_read(r, s_cfg->session_key, MODSSO_SESSION_KEY_SIZE,
                              &unique_id, sso_login_path) < 0) {
        ap_log_error(APLOG_MARK, APLOG_INFO, 0, r->server,
                     "sso: could not read session cookie");
        // Instead of returning 400 here, we can try to improve the
        // user experience by redirecting them to the 'redir' endpoint
        // without setting the SSO cookie (hoping that whatever the
        // reason for the lack of session cookie, it is a temporary
        // issue). This creates the potential for a redirect loop.
        return http_redirect(r, redir);
      }
      if ((err = sso_ticket_open(&t, params.t, s_cfg->public_key)) != SSO_OK) {
        ap_log_error(APLOG_MARK, APLOG_WARNING, 0, r->server,
                     "sso: ticket decoding error: %s, tkt=%s",
                     sso_strerror(err), params.t);
        return HTTP_BAD_REQUEST;
      }
      // TODO: add group validation. Not really a huge deal, since we're
      // going to re-validate the token on the original endpoint anyway,
      // but it would maybe be good for clarity.
      if ((err = sso_validate(t, service, s_cfg->domain, unique_id, NULL)) !=
          SSO_OK) {
        ap_log_error(APLOG_MARK, APLOG_WARNING, 0, r->server,
                     "sso: ticket validation error: %s (nonce=%s, t->nonce=%s)",
                     sso_strerror(err), unique_id, t->nonce);
        return HTTP_BAD_REQUEST;
      }
    }

    modsso_set_cookie(r, sso_cookie_name, params.t, service_path);

    return http_redirect(r, redir);
  }

  return DECLINED;
}

static int ends_with_slash(const char *s) {
  int l = strlen(s);
  if (l < 1) {
    return 0;
  }
  return s[l-1] == '/';
}

static int redirect_to_login_server(request_rec *r, modsso_config *s_cfg,
                                    const char *service_host,
                                    const char *service, const char **groups,
                                    const char *service_path) {
  char *dest, *login_url, *sso_login_path;
  const char *unique_id;

  dest = full_uri(r, service_host);
  login_url = apr_pstrcat(r->pool, "https://", s_cfg->login_server,
                          ends_with_slash(s_cfg->login_server) ? "" : "/",
                          "?s=", modsso_url_encode(r->pool, service),
                          "&d=", modsso_url_encode(r->pool, dest), NULL);
  if (s_cfg->session_key != NULL) {
    // If we have a session key, send a nonce to the login server. We
    // use mod_unique_id to provide us with a unique token. The fact
    // that it is predictable is not a big deal, we're sending it out
    // in cleartext anyway (what matters is that it is saved locally
    // in the signed session).
    unique_id = apr_table_get(r->subprocess_env, "UNIQUE_ID");
    if (unique_id != NULL) {
      ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, r->server,
                   "sso: generated unique id %s", unique_id);
      login_url = apr_pstrcat(r->pool, login_url, "&n=", unique_id, NULL);
      // Set the cookie path to the /sso_login handler only, to avoid
      // sending the session cookie on every unrelated request.
      // Ignore errors here, not much else we can do.
      sso_login_path = apr_pstrcat(r->pool, service_path, "sso_login", NULL);
      modsso_session_save(r, s_cfg->session_key, MODSSO_SESSION_KEY_SIZE,
                          unique_id, sso_login_path);
    }
  }
  if (groups) {
    login_url =
        apr_pstrcat(r->pool, login_url,
                    "&g=", groups_charp_to_string(r->pool, groups), NULL);
  }
  ap_log_error(APLOG_MARK, APLOG_INFO, 0, r->server,
               "sso: unauthorized access to %s", dest);
  ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, r->server, "sso: redirecting to %s",
               login_url);
  return http_redirect(r, login_url);
}

/**
 * Apache authentication handler for mod_sso.
 *
 * @param r Pointer to the request_rec structure.
 */
static int mod_sso_check_access_ex(request_rec *r) {
  const char *uri;
  const char *sso_login_path, *sso_logout_path;
  const char *service = NULL, *service_host = NULL, *service_path = NULL;
  modsso_config *s_cfg =
      (modsso_config *)ap_get_module_config(r->per_dir_config, &sso_module);

  if (!is_sso_auth(r)) {
    return DECLINED;
  }

  // Check if the required parameters are defined.
  if (!check_config(r, s_cfg)) {
    return HTTP_INTERNAL_SERVER_ERROR;
  }

  uri = r->uri;

  if (parse_service(r, s_cfg, &service, &service_host, &service_path) != 0) {
    ap_log_error(
        APLOG_MARK, APLOG_ERR, 0, r->server,
        "sso (check_access_ex): could not parse service (cfg->service=%s)",
        s_cfg->service);
    return HTTP_BAD_REQUEST;
  }

  // Everyone is allowed access to /sso_login and /sso_logout
  sso_logout_path = apr_pstrcat(r->pool, service_path, "sso_logout", NULL);
  sso_login_path = apr_pstrcat(r->pool, service_path, "sso_login", NULL);
  if (!strcmp(uri, sso_logout_path) || !strcmp(uri, sso_login_path)) {
    ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, r->server,
                 "allowing access to /sso_login or /sso_logout handler");
    return OK;
  }

  return DECLINED;
}

static int mod_sso_check_user_id(request_rec *r) {
  const char *sso_cookie_name, *sso_cookie;
  const char *service = NULL, *service_host = NULL, *service_path = NULL;
  int retval, err, do_redirect = 1;
  const char **required_groups;
  modsso_config *s_cfg =
      (modsso_config *)ap_get_module_config(r->per_dir_config, &sso_module);
  // apr_array_header_t *sso_validate_groups = NULL;

  if (!is_sso_auth(r)) {
    return DECLINED;
  }

  // If this is a sub-request, pass existing credentials, if any.
  if (!ap_is_initial_req(r)) {
    if (r->main != NULL) {
      r->user = r->main->user;
    } else if (r->prev != NULL) {
      r->user = r->prev->user;
    }
    if (r->user != NULL) {
      return OK;
    }
  }

  sso_cookie_name = get_cookie_name(r);

  // Check if the required parameters are defined.
  if (!check_config(r, s_cfg)) {
    return HTTP_INTERNAL_SERVER_ERROR;
  }

  if (parse_service(r, s_cfg, &service, &service_host, &service_path) != 0) {
    ap_log_error(
        APLOG_MARK, APLOG_ERR, 0, r->server,
        "sso (check_user_id): could not parse service (cfg->service=%s)",
        s_cfg->service);
    return HTTP_BAD_REQUEST;
  }

  // Fetch the list of desired groups set (eventually) by
  // group_check_authorization.
  required_groups =
      (const char **)apr_table_get(r->notes, "SSO_REQUIRED_GROUPS");

  // Test for valid cookie
  sso_cookie = modsso_get_cookie(r, sso_cookie_name);
  if (sso_cookie != NULL) {
    sso_ticket_t t;

    err = sso_ticket_open(&t, sso_cookie, s_cfg->public_key);
    if (err != SSO_OK) {
      ap_log_error(APLOG_MARK, APLOG_WARNING, 0, r->server,
                   "sso: ticket decoding error: %s, tkt=%s", sso_strerror(err),
                   sso_cookie);
    } else {
      err = sso_validate(t, service, s_cfg->domain, NULL, required_groups);
      if (err != SSO_OK) {
        ap_log_error(APLOG_MARK, APLOG_WARNING, 0, r->server,
                     "sso: validation error: %s", sso_strerror(err));
      } else {
        // Don't do this: t->groups wil be freed by sso_ticket_free
        // apr_table_setn(r->notes, "SSO_GROUPS", (char *)(t->groups));
        apr_table_setn(r->subprocess_env, "SSO_SERVICE",
                       apr_pstrdup(r->pool, service));
        r->user = apr_pstrdup(r->pool, t->user);
        apr_table_setn(r->subprocess_env, "SSO_USER", r->user);
        apr_table_setn(r->subprocess_env, "SSO_TICKET", sso_cookie);
        if (t->nonce)
          apr_table_setn(r->subprocess_env, "SSO_NONCE",
                         apr_pstrdup(r->pool, t->nonce));
        ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, r->server,
                     "sso: authorized user '%s'", r->user);
        retval = OK;
        do_redirect = 0;
      }
      sso_ticket_free(t);
    }
  }

  if (!do_redirect) {
    return retval;
  }

  // Redirect to login server
  return redirect_to_login_server(r, s_cfg, service_host, service,
                                  required_groups, service_path);
}

/**
 * Apache authorization check callback for mod_sso.
 */
static char **groups_array_to_charpp(apr_pool_t *p,
                                     apr_array_header_t *groups) {
  int i;
  char **pp, **ptr;
  pp = (char **)apr_palloc(p, sizeof(char *) * (groups->nelts + 1));
  for (ptr = pp, i = 0; i < groups->nelts; i++) {
    *ptr++ = APR_ARRAY_IDX(groups, i, char *);
  }
  *ptr = NULL;
  return pp;
}

static apr_array_header_t *
required_groups_array(request_rec *r, const void *parsed_require_args) {
  const ap_expr_info_t *expr = parsed_require_args;
  const char *err = NULL;
  const char *require, *w, *t;
  apr_array_header_t *grouparr = apr_array_make(r->pool, 1, sizeof(char *));

  require = ap_expr_str_exec(r, expr, &err);
  if (err) {
    return NULL;
  }

  t = require;
  while ((w = ap_getword_conf(r->pool, &t)) && w[0]) {
    *(const char **)apr_array_push(grouparr) = w;
  }
  return grouparr;
}

static char **required_groups_charpp(request_rec *r,
                                     const void *parsed_require_args) {
  apr_array_header_t *arr = required_groups_array(r, parsed_require_args);
  if (!arr) {
    return NULL;
  }
  return groups_array_to_charpp(r->pool, arr);
}

// This function will be called twice: first, before any authn
// handlers are executed, and we return AUTHZ_DENIED_NO_USER to tell
// Apache that we need a user. This should cause it to invoke
// mod_sso_check_user_id, and then call this function again.
static authz_status group_check_authorization(request_rec *r,
                                              const char *require_args,
                                              const void *parsed_require_args) {
  // Do we have a user? All ok then! We assume that the request was
  // validated by mod_sso_check_user_id using the value of
  // SSO_REQUIRED_GROUPS we set earlier.
  if (r->user) {
    return AUTHZ_GRANTED;
  }

  // Set the list of groups in the request notes, so that the
  // sso authn handler can retrieve it and validate the ticket.
  apr_table_setn(r->notes, "SSO_REQUIRED_GROUPS",
                 (char *)required_groups_charpp(r, parsed_require_args));
  return AUTHZ_DENIED_NO_USER;
}

static const char *group_parse_config(cmd_parms *cmd, const char *require_line,
                                      const void **parsed_require_line) {
  const char *expr_err = NULL;
  ap_expr_info_t *expr;

  expr = ap_expr_parse_cmd(cmd, require_line, AP_EXPR_FLAG_STRING_RESULT,
                           &expr_err, NULL);

  if (expr_err) {
    return apr_pstrcat(cmd->temp_pool,
                       "Cannot parse expression in require line: ", expr_err,
                       NULL);
  }

  *parsed_require_line = expr;

  return NULL;
}

static const authz_provider authz_sso_group_provider = {
    &group_check_authorization,
    &group_parse_config,
};

/**
 * Apache register_hooks callback for mod_sso.
 *
 * This function is a callback and it declares what other functions
 * should be called for request processing and configuration
 * requests. This callback function declares the Handlers for other
 * events.
 */
static void mod_sso_register_hooks(apr_pool_t *p) {
  ap_hook_handler(mod_sso_method_handler, NULL, NULL, APR_HOOK_FIRST);
  ap_hook_check_authn(mod_sso_check_user_id, NULL, NULL, APR_HOOK_FIRST,
                      AP_AUTH_INTERNAL_PER_CONF);
  ap_hook_check_access_ex(mod_sso_check_access_ex, NULL, NULL, APR_HOOK_MIDDLE,
                          AP_AUTH_INTERNAL_PER_CONF);
  ap_register_auth_provider(p, AUTHZ_PROVIDER_GROUP, "group", "0",
                            &authz_sso_group_provider,
                            AP_AUTH_INTERNAL_PER_CONF);
}

/*
 * Declare and populate the module's data structure.  The name of this
 * structure ('sso_module') is important - it must match the name of
 * the module.  This structure is the only "glue" between the httpd
 * core and the module.
 */
module AP_MODULE_DECLARE_DATA sso_module = {
    STANDARD20_MODULE_STUFF,
    create_modsso_config,
    merge_modsso_config,
    NULL,
    NULL,
    mod_sso_cmds,
    mod_sso_register_hooks,
};
