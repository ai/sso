/* Copyright (c) 2012 Autistici/Inventati <info@autistici.org>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include "ap_config.h"
#include "apr_strings.h"
#include <sso/sso.h>

#ifdef APLOG_USE_MODULE
APLOG_USE_MODULE(sso);
#endif

/* overwrite package vars set by apache */
#undef PACKAGE_BUGREPORT
#undef PACKAGE_NAME
#undef PACKAGE_STRING
#undef PACKAGE_TARNAME
#undef PACKAGE_VERSION
#include "config.h"

#define MODSSO_SESSION_KEY_SIZE 32

struct request_rec;

struct modsso_params {
  char *t;
  char *d;
};

typedef struct modsso_params *modsso_params_t;

// sso_utils.c
const char *modsso_url_decode(apr_pool_t *p, const char *str);
const char *modsso_url_encode(apr_pool_t *p, const char *str);

int modsso_parse_query_string(apr_pool_t *p, const char *str,
                              modsso_params_t params);

char *modsso_get_cookie(request_rec *r, const char *cookie_name);

void modsso_set_cookie(request_rec *r, const char *cookie_name,
                       const char *value, const char *path);

void modsso_del_cookie(request_rec *r, const char *cookie_name,
                       const char *path);
int modsso_read_fixed_size_file(apr_pool_t *p, const char *path,
                                size_t size, const unsigned char **out);

// session.c
int modsso_session_generate_temp_key(apr_pool_t *pool, unsigned char *out,
                                     size_t *outsz);
int modsso_session_deserialize(apr_pool_t *pool, const unsigned char *key,
                               size_t key_len, const char *serialized_value,
                               char **value, size_t *value_len);
int modsso_session_serialize(apr_pool_t *pool, const unsigned char *key,
                             size_t key_len, const char *value,
                             size_t value_len, char **output);
int modsso_session_read(request_rec *r, const unsigned char *key,
                        size_t key_len, char **value, const char *cookie_path);
int modsso_session_save(request_rec *r, const unsigned char *key,
                        size_t key_len, const char *unique_id,
                        const char *path);
