
#include <stdlib.h>
#include <gtest/gtest.h>
#include <string>

extern "C" {
#include "../mod_sso.h"
#include "httpd.h"
}

using std::string;

namespace {

class SSOUtilsTest : public testing::Test {
protected:

  virtual void SetUp() {
    apr_pool_create(&pool_, NULL);
  }

  apr_pool_t *pool_;
};


TEST_F(SSOUtilsTest, UrlDecode) {
  string sample("%76%61%6Cue"), reference("value");
  string result = modsso_url_decode(pool_, sample.c_str());
  EXPECT_EQ(reference, result);

  string bad_input("%%%%%%%%%");
  result = modsso_url_decode(pool_, bad_input.c_str());
  EXPECT_EQ(bad_input, result);

  bad_input = "%az%AZ%Z%Z";
  result = modsso_url_decode(pool_, bad_input.c_str());
  EXPECT_EQ(bad_input, result);
}

TEST_F(SSOUtilsTest, UrlEncode) {
  string sample("http://example.com"),
    reference("http%3a%2f%2fexample.com");
  string result = modsso_url_encode(pool_, sample.c_str());
  EXPECT_EQ(reference, result);
}

TEST_F(SSOUtilsTest, ParseQueryString) {
  modsso_params parms;
  ASSERT_EQ(0, modsso_parse_query_string(pool_, "t=val1&d=%76%61%6C2", &parms));
  EXPECT_EQ("val1", string(parms.t));
  EXPECT_EQ("val2", string(parms.d)) << "URL-decoding failed";
  ASSERT_EQ(-1, modsso_parse_query_string(pool_, "key3=", &parms));
}

//TEST_F(SSOUtilsTest, MakeCookieValue) {
//  string cookie;
//  modsso::make_cookie_value(cookie, "COOKIE", "value", "/", 0);
//  ASSERT_EQ("COOKIE=value; path=/; secure;", cookie);
//}

TEST_F(SSOUtilsTest, GetCookieEasy) {
  request_rec r;
  r.pool = pool_;
  r.headers_in = apr_table_make(pool_, 1);
  const char *headers[] = {
    "SSO=1234", "other=blah; SSO=1234", "SSO_other=3456; SSO=1234", "other=blah; SSO=1234; more=more",
  };
  int i;

  for (i = 0; i < (int)(sizeof(headers) / sizeof(char *)); i++) {
    apr_table_set(r.headers_in, "Cookie", headers[i]);

    const char *cookie = modsso_get_cookie(&r, "SSO");
    ASSERT_TRUE(cookie) << "cookie not found in: " << headers[i];
    ASSERT_EQ("1234", string(cookie))
      << "bad cookie parsed from " << headers[i] << ": " << cookie;
  }
}
  
} // namespace

int main(int argc, char **argv) {
  //apr_app_initialize(&argc, &argv, NULL);
  apr_initialize();
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

