
#include <stdlib.h>
#include <gtest/gtest.h>
#include <string>

extern "C" {
#include "../mod_sso.h"
}

using std::string;

namespace {

class SessionTest : public testing::Test {
protected:
  virtual void SetUp() {
    apr_pool_create(&pool_, NULL);
    key_len_ = MODSSO_SESSION_KEY_SIZE;
    key_ = (unsigned char *)apr_pcalloc(pool_, key_len_);
    modsso_session_generate_temp_key(pool_, key_, &key_len_);
  }

  apr_pool_t *pool_;
  unsigned char *key_;
  size_t key_len_;
};

TEST_F(SessionTest, Serialize) {
  const char *values[] = {
    "a",
    "ab",
    "abc",
    "abcd",
    "abcde",
    "abcdef",
    "abcdefg",
    "abcdefgh",
    "abcdefghi",
    "WokecMCoCgsAAAATnNUAAABA",
    NULL,
  };
  char *output = NULL;

  for (int i = 0; values[i]; i++) {
    ASSERT_EQ(0, modsso_session_serialize(pool_, key_, key_len_, values[i],
                                          strlen(values[i]), &output));
    ASSERT_STRNE(NULL, output);
    ASSERT_STRNE(NULL, strchr(output, '.'));

    char *value2 = NULL;
    size_t value2_len = 0;
    ASSERT_EQ(0, modsso_session_deserialize(pool_, key_, key_len_, output,
                                            &value2, &value2_len));
    ASSERT_STRNE(NULL, value2);
    ASSERT_NE((size_t)0, value2_len);

    ASSERT_STREQ(value2, values[i]);
  }
}

} // namespace

int main(int argc, char **argv) {
  apr_initialize();
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
