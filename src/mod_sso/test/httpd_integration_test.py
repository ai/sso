#!/usr/bin/python

import os
import re
import requests
import subprocess
import sys
import time
import unittest
from urllib.parse import urlencode, urlsplit, parse_qsl

sys.path.append("../../python")
import sso

# Allow overriding the apache2 executable location from the environment.
APACHE_BIN = os.getenv('APACHE_BIN', '/usr/sbin/apache2')
APXS_BIN = os.getenv('APXS_BIN', '/usr/bin/apxs2')
for exe in (APACHE_BIN, APXS_BIN):
    if not os.path.exists(exe):
        raise unittest.SkipTest('%s not found' % exe)

# The port that apache2 will listen on.
APACHE_PORT = int(os.getenv('APACHE_PORT', '33000'))

# A temporary file used for logging.
APACHE_LOG = os.path.join(os.getcwd(), '.apache_log')

devnull = open(os.devnull)


def _start_httpd(public_key, config_file):
    with open('public.key', 'wb') as fd:
        fd.write(public_key)
    env = dict(os.environ)
    env['TESTROOT'] = os.getcwd()
    env['MODULEDIR'] = subprocess.check_output(
        [APXS_BIN, '-q', 'LIBEXECDIR'], stderr=devnull).strip()
    env['APACHE_PORT'] = str(APACHE_PORT)
    env['APACHE_LOG'] = APACHE_LOG
    try:
        os.remove(APACHE_LOG)
    except:
        pass
    cmd = [APACHE_BIN, "-f", os.path.join(os.getcwd(), config_file), "-X"]

    if os.getenv('STRACE'):
        cmd = ['strace', '-s', '256', '-f'] + cmd
    if os.getenv('VALGRIND'):
        cmd = ['valgrind'] + cmd

    httpd = subprocess.Popen(cmd, env=env)
    #print 'httpd pid:', httpd.pid
    time.sleep(0.5)
    if httpd.poll():
        raise Exception('httpd failed to start!')
    return httpd


def _stop_httpd(httpd, dump_log=False):
    httpd.terminate()
    time.sleep(1)
    try:
        httpd.kill()
    except OSError:
        pass

    if os.getenv('DUMP_APACHE_LOG'):
        dump_log = True
    status = httpd.wait()
    if os.WIFEXITED(status):
        if os.WEXITSTATUS(status) != 0:
            print('WARNING: httpd exited with status %d' % os.WEXITSTATUS(status))
            dump_log = True
    elif os.WIFSIGNALED(status):
        print('WARNING: httpd exited due to signal %d' % os.WTERMSIG(status))
        dump_log = True
    else:
        print('WARNING: httpd exited for unknown reason (returncode=%d)' % status)
        dump_log = True

    if dump_log:
        print('*** APACHE LOG ***')
        with open(APACHE_LOG) as fd:
            for line in fd:
                print(line.rstrip('\n'))

    for f in ('public.key', 'test-httpd.pid', '.apache_log'):
        try:
            os.remove(f)
        except OSError:
            pass


def _query(url, host=None, cookie=None):
    """Make a simple request to url using httplib."""
    headers = {"Host": host or "localhost"}
    if cookie:
        headers['Cookie'] = cookie
    resp = requests.get(
        f'http://127.0.0.1:{APACHE_PORT}{url}',
        headers=headers,
        allow_redirects=False)
    location = None
    body = None
    if resp.status_code in (301, 302):
        location = resp.headers["Location"]
    elif resp.status_code == 200:
        body = resp.text
    return (resp.status_code, body, location)


def mkcookie(tkt):
    return "SSO_test=%s" % tkt


class HttpdKeyLoadingTest(unittest.TestCase):

    def test_key_with_null_byte(self):
        has_null_byte = lambda s: 0 in s

        public = b''
        while not has_null_byte(public):
            public, secret = sso.generate_keys()

        httpd = _start_httpd(public, 'test-httpd-2.4.conf')
        try:
            signer = sso.Signer(secret)
            t = sso.Ticket("testuser", "service.example.com/", "example.com",
                           groups=set(["group1"]))
            ts = signer.sign(t)
            status, body, location = _query("/index.html", 
                                            cookie="SSO_test=%s" % ts)
            self.assertEquals(200, status)
        finally:
            _stop_httpd(httpd)


class HttpdIntegrationTestBase(unittest.TestCase):

    CONFIG = None
    DUMP_LOG = False

    def setUp(self):
        self.public, self.secret = sso.generate_keys()
        self.signer = sso.Signer(self.secret)
        self.httpd = _start_httpd(self.public, self.CONFIG)

    def tearDown(self):
        _stop_httpd(self.httpd, self.DUMP_LOG)

    def _ticket(self, user="testuser", group="group1", service="service.example.com/",
                nonce=None):
        t = sso.Ticket(user, service, "example.com", groups=set([group]), nonce=nonce)
        signedt = self.signer.sign(t)
        #print 'ticket:', signedt
        return signedt

    def _test_auth(self, use_nonce=False, repeats=2):
        # Single-request checks that exercise the authentication logic
        # in mod_sso.

        # Set to a non-empty string when testing the SSOGroups directive
        # (normally only the requested groups are generated).
        #extra_groups = "&g=group1,group2,group3"
        extra_groups = ''

        # Regexp to ignore the 'n' parameter (which is random) in
        # response redirects when use_nonce is True.
        drop_nonce_rx = re.compile(r'&n=[-_a-zA-Z0-9@]+')

        # Tests have a name so that we can recognize failures.
        checks = [
            ("index -> redirect",
             {"url": "/index.html", 
              "status": 302, 
              "location": "https://login.example.com/?s=service.example.com%2f&d=https%3a%2f%2fservice.example.com%2findex.html" + extra_groups}),
            ("index with cookie -> ok",
             {"url": "/index.html",
              "cookie": mkcookie(self._ticket()),
              "status": 200,
              "body": "ok"}),
            ("index with malformed cookie -> redirect",
             {"url": "/index.html",
              "cookie": mkcookie('blahblah' * 8),
              "status": 302,
              "location": "https://login.example.com/?s=service.example.com%2f&d=https%3a%2f%2fservice.example.com%2findex.html" + extra_groups}),

            ("protected-user -> redirect",
             {"url": "/protected-user/index.html", 
              "status": 302, 
              "location": "https://login.example.com/?s=service.example.com%2f&d=https%3a%2f%2fservice.example.com%2fprotected-user%2findex.html" + extra_groups}),
            ("protected-user with cookie -> ok",
             {"url": "/protected-user/index.html",
              "cookie": mkcookie(self._ticket()),
              "status": 200,
              "body": "ok"}),
            ("protected-user with cookie wrong user -> fail",
             {"url": "/protected-user/index.html",
              "cookie": mkcookie(self._ticket(user="wronguser")),
              "status": 401 }),

            ("protected-group -> redirect",
             {"url": "/protected-group/index.html", 
              "status": 302, 
              "location": "https://login.example.com/?s=service.example.com%2f&d=https%3a%2f%2fservice.example.com%2fprotected-group%2findex.html" + (extra_groups if extra_groups else "&g=group1")}),
            ("protected-group with cookie -> ok",
             {"url": "/protected-group/index.html",
              "cookie": mkcookie(self._ticket()),
              "status": 200,
              "body": "ok"}),
            #("protected-group with cookie wrong group -> unauthorized",
            # {"url": "/protected-group/index.html",
            #  "cookie": mkcookie(self._ticket(group="group2")),
            #  "status": 401}),
            ("protected-group with cookie wrong group -> redirect",
             {"url": "/protected-group/index.html",
              "cookie": mkcookie(self._ticket(group="group2")),
              "status": 302,
              "location": "https://login.example.com/?s=service.example.com%2f&d=https%3a%2f%2fservice.example.com%2fprotected-group%2findex.html" + (extra_groups if extra_groups else "&g=group1")}),

            ("other-service -> redirect",
             {"url": "/other-service/index.html",
              "status": 302,
              "http_host": "testhost.example.com",
              "location": "https://login.example.com/?s=testhost.example.com%2fother-service%2f&d=https%3a%2f%2ftesthost.example.com%2fother-service%2findex.html" + extra_groups}),

            ("protected-htaccess -> redirect",
             {"url": "/protected-htaccess/index.html",
              "status": 302,
              "location": "https://login.example.com/?s=service.example.com%2fprotected-htaccess%2f&d=https%3a%2f%2fservice.example.com%2fprotected-htaccess%2findex.html" + extra_groups}),
            ("protected-htaccess with cookie -> ok",
             {"url": "/protected-htaccess/index.html",
              "cookie": mkcookie(self._ticket(service="service.example.com/protected-htaccess/")),
              "status": 200,
              "body": "ok"}),
            ("protected-htaccess with cookie wrong user -> fail",
             {"url": "/protected-htaccess/index.html",
              "cookie": mkcookie(self._ticket(user="wronguser", service="service.example.com/protected-htaccess/")),
              "status": 401 }),
            ]
        for name, check in checks:
            for i in range(repeats):
                print('CHECKING %s (%d of 10)' % (name, i), check)
                status, body, location = _query(check["url"],
                                                host=check.get("http_host"),
                                                cookie=check.get("cookie"))
                if use_nonce and location:
                    location = drop_nonce_rx.sub('', location)
                self.assertEquals(
                    check["status"], status,
                    "test: '%s'\nunexpected HTTP status for %s (got %d, exp %d)" % (
                        name, check["url"], status, check["status"]))
                if "body" in check:
                    self.assertTrue(
                        check["body"] in body,
                        "test: '%s'\nbody mismatch for %s (exp '%s')" % (
                            name, check["url"], check["body"]))
                if "location" in check:
                    self.assertEquals(
                        check["location"], location,
                        "test: '%s'\nunexpected redirect for %s (got %s, exp %s)" % (
                            name, check["url"], location, check["location"]))

    def _login_workflow(self, host_header, url, sso_service=None, sso_login_url=None):
        if not sso_service:
            sso_service = host_header + os.path.dirname(url)
        if not sso_service.endswith('/'):
            sso_service += '/'
        if not sso_login_url:
            sso_login_url = '/%s/sso_login' % sso_service.split('/', 1)[1]

        sess = requests.Session()

        # Make a request to a protected URL.
        resp = sess.get(
            f'http://127.0.0.1:{APACHE_PORT}{url}',
            headers={'Host': host_header},
            allow_redirects=False)
        self.assertEquals(302, resp.status_code)
        location = resp.headers["Location"]
        location_u = urlsplit(location)
        location_args = dict(parse_qsl(location_u.query))
        nonce = location_args.get('n')
        self.assertTrue(nonce)
        session_cookie = resp.cookies['_sso_local_session']

        # Now call the /sso_login endpoint.
        tkt = self._ticket(nonce=nonce, service=sso_service)
        query_args = urlencode({"t": tkt, "d": "https://" + host_header + url})
        resp = sess.get(
            f'http://127.0.0.1:{APACHE_PORT}{sso_login_url}?{query_args}',
            headers={'Host': host_header},
            cookies={'_sso_local_session': session_cookie},
            allow_redirects=False)
        self.assertEquals(302, resp.status_code)
        self.assertTrue('SSO_test' in resp.cookies,
                        'missing cookie: %s\n%s' % (resp.cookies, resp.headers))
        self.assertEquals(tkt, resp.cookies['SSO_test'])

        # Make the original request again.
        resp = sess.get(
            f'http://127.0.0.1:{APACHE_PORT}{url}',
            headers={'Host': host_header},
            cookies={'SSO_test': tkt},
            allow_redirects=False)
        self.assertEquals(200, resp.status_code)


class HttpdIntegrationTest(HttpdIntegrationTestBase):

    CONFIG = 'test-httpd-2.4.conf'

    def test_auth(self):
        self._test_auth()

    def test_many_requests(self):
        # Repeat a simple request many times. Useful with valgrind and
        # to spot issues where we are not cleaning up state properly
        # between requests.
        n = 100
        for i in range(n):
            cookie = 'SSO_test=%s' % self._ticket()
            status, body, location = _query("/index.html", cookie=cookie)
            self.assertEquals(200, status)

    def test_cgi_environment(self):
        # test that environment variables are correctly set
        tkt = self._ticket(nonce='NONCE1')
        status, body, location = _query("/cgi/env.cgi",
                                        cookie=mkcookie(tkt))
        self.assertEquals(200, status)
        self.assertTrue(body)
        self.assertTrue("REMOTE_USER=testuser" in body)
        self.assertTrue("SSO_USER=testuser" in body)
        self.assertTrue("SSO_SERVICE=service.example.com/" in body)
        self.assertTrue(("SSO_TICKET=" + tkt) in body)

    def test_sso_login(self):
        # Call the /sso_login endpoint, verify that it sets the local
        # SSO cookie to whatever the 't' parameter says.
        tkt = self._ticket()
        query_args = urlencode(
            {"t": tkt, "d": "https://service.example.com/index.html"})
        resp = requests.get(
            f'http://127.0.0.1:{APACHE_PORT}/sso_login?{query_args}',
            allow_redirects=False)
        self.assertEquals(302, resp.status_code)
        set_cookie_hdr = resp.headers["Set-Cookie"]
        self.assertTrue(set_cookie_hdr)
        self.assertTrue('SSO_test' in resp.cookies,
                        'missing cookie: %s' % resp.cookies)
        self.assertEquals(tkt, resp.cookies['SSO_test'])

    def test_sso_logout(self):
        # test the /sso_logout endpoint
        resp = requests.get(
            f'http://127.0.0.1:{APACHE_PORT}/sso_logout',
            headers={"Cookie": mkcookie(self._ticket())},
            allow_redirects=False)
        set_cookie_hdr = resp.headers["Set-Cookie"]
        self.assertTrue(set_cookie_hdr)
        self.assertTrue("SSO_test=;" in set_cookie_hdr)


class HttpdIntegrationTestWithNonces(HttpdIntegrationTestBase):

    CONFIG = 'test-httpd-2.4-nonces.conf'

    def setUp(self):
        with open('session.key', 'wb') as fd:
            fd.write(os.urandom(32))
        HttpdIntegrationTestBase.setUp(self)

    def test_auth(self):
        self._test_auth(True)

    def test_cgi_environment(self):
        # test that environment variables are correctly set
        tkt = self._ticket(nonce='NONCE1')
        status, body, location = _query("/cgi/env.cgi",
                                        cookie=mkcookie(tkt))
        self.assertEquals(200, status)
        self.assertTrue(body)
        self.assertTrue("REMOTE_USER=testuser" in body)
        self.assertTrue("SSO_USER=testuser" in body)
        self.assertTrue("SSO_SERVICE=service.example.com/" in body)
        self.assertTrue(("SSO_TICKET=" + tkt) in body)
        self.assertTrue("SSO_NONCE=NONCE1" in body)

    def test_login_workflow(self):
        self._login_workflow("service.example.com", "/protected-user/index.html",
                             sso_service="service.example.com/")

    def test_login_workflow_group(self):
        self._login_workflow("service.example.com", "/other-service/index.html",
                             sso_service="service.example.com/other-service/",
                             sso_login_url="/other-service/sso_login")


# Test a specific top-level configuration, called 'webmail' for historical reasons.
class WebmailIntegrationTestWithNonces(HttpdIntegrationTestBase):

    CONFIG = 'test-httpd-2.4-webmail.conf'
    DUMP_LOG = True

    def setUp(self):
        with open('session.key', 'wb') as fd:
            fd.write(os.urandom(32))
        HttpdIntegrationTestBase.setUp(self)

    def test_login_workflow(self):
        self._login_workflow("service.example.com", "/index.html")


class SubdirIntegrationTest(HttpdIntegrationTestBase):

    CONFIG = 'test-httpd-subdir.conf'

    def test_subdir(self):
        self._login_workflow("service.example.com", "/protected/index.html")

    
if __name__ == "__main__":
    unittest.main()

