import os

from distutils.core import setup, Extension
from distutils.command.build_py import build_py as _build_py

SRCDIR = os.getenv('SRCDIR', '.')
BUILDDIR = os.getenv('BUILDDIR', SRCDIR)
SSO_LIB_DIR = os.getenv('SSO_LIB_DIR', '.')
top_srcdir = os.path.join(SRCDIR, '..')

# If BUILD_MODULE is not defined, bundle libsso into the Python
# extension so that this package is pip-installable directly.
ext_sources = ['sso/sso_python.c']
libraries = ['sso']
if not os.getenv('BUILD_MODULE'):
    ext_sources.extend(os.path.join(top_srcdir, 'sso', x) for x in [
        'base64.c', 'randombytes.c', 'sso.c', 'tweetnacl.c',
    ])
    libraries = []


# Run the build_ext command first (swig generates a Python file).
class build_py(_build_py):
    def run(self):
        self.run_command('build_ext')
        return _build_py.run(self)


setup(name='sso',
      cmdclass={'build_py': build_py},
      ext_modules=[Extension('sso._pysso', ext_sources,
                             #swig_opts=['-modern', '-I' + top_srcdir, '-py3'],
                             include_dirs=[top_srcdir],
                             libraries=libraries,
                             library_dirs=[SSO_LIB_DIR],
                            )],
      package_dir={'sso': 'sso'},
      packages=['sso'],
)
