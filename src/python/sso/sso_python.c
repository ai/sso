#include "sso/sso.h"
#include <stdlib.h>

#define PY_SSIZE_T_CLEAN
#include "py3compat.h"
#include <Python.h>

#define PYSSO_STATIC_SIGN_BUFSIZE 1024

static const char **parse_groups(PyObject *groups_obj) {
  int bufalloc = 8, i = 0;
  char **buf = (char **)malloc(sizeof(char *) * bufalloc);
  PyObject *iter, *item;

  iter = PyObject_GetIter(groups_obj);
  if (!iter) {
    return NULL;
  }
  while ((item = PyIter_Next(iter))) {
    buf[i++] = PyStr_AsString(item);
    if (i >= bufalloc - 1) {
      bufalloc *= 2;
      buf = (char **)realloc(buf, sizeof(char *) * bufalloc);
    }
    Py_DECREF(item);
  }
  buf[i] = NULL;
  Py_DECREF(iter);
  return (const char **)buf;
}

// Convert a list of groups to a Python list. If groups is NULL,
// returns an empty list.
static PyObject *groups_to_list(char **groups) {
  PyObject *groups_obj;
  char **p;
  Py_ssize_t n = 0;
  int i;

  for (p = groups; p && *p; p++) {
    n++;
  }

  groups_obj = PyList_New(n);
  for (i = 0; i < n; i++) {
    PyList_SET_ITEM(groups_obj, i, PyStr_FromString(groups[i]));
  }
  return groups_obj;
}

static PyObject *pysso_error;

static void pysso_set_error(int err) {
  PyErr_SetString(pysso_error, sso_strerror(err));
}

// create_and_sign_ticket(private_key, user, service, domain, [nonce, groups,
// ttl])
static PyObject *pysso_create_and_sign_ticket(PyObject *self, PyObject *args,
                                              PyObject *kwargs) {
  const char *priv_key = NULL;
  Py_ssize_t priv_key_size = 0;
  const char *user = NULL;
  const char *service = NULL;
  const char *domain = NULL;
  const char *nonce = NULL;
  PyObject *groups_obj = NULL;
  const char **groups = NULL;
  int ttl = 3600;
  static char *kwlist[] = {"private_key", "user",   "service", "domain",
                           "nonce",       "groups", "ttl",     NULL};

  PyObject *result_obj = NULL;
  sso_ticket_t ticket = NULL;
  char buf[PYSSO_STATIC_SIGN_BUFSIZE]; /* on-stack buffer for signed output */
  int err;

  if (!PyArg_ParseTupleAndKeywords(args, kwargs,
#if IS_PY3
                                   "y#sss|zOi:create_and_sign_ticket",
#else
                                   "s#sss|zOi:create_and_sign_ticket",
#endif
                                   kwlist, &priv_key, &priv_key_size, &user,
                                   &service, &domain, &nonce, &groups_obj,
                                   &ttl)) {
    return NULL;
  }

  if (priv_key_size != SSO_SECRET_KEY_SIZE) {
    PyErr_SetString(PyExc_ValueError, "Bad secret key size");
    goto fail;
  }

  if (groups_obj && groups_obj != Py_None) {
    groups = parse_groups(groups_obj);
  }

  ticket = sso_ticket_new(user, service, domain, nonce, groups, ttl);
  if (!ticket) {
    PyErr_SetString(PyExc_ValueError, "Invalid ticket parameters");
    goto fail;
  }

  err = sso_ticket_sign(ticket, (const unsigned char *)priv_key, buf,
                        sizeof(buf));
  if (err != SSO_OK) {
    pysso_set_error(err);
    goto fail;
  }

  result_obj = PyStr_FromString(buf);

fail:
  if (groups) {
    free(groups);
  }
  if (ticket) {
    sso_ticket_free(ticket);
  }
  return result_obj;
}

// verify_ticket(public_key, ticket, service, domain, [nonce, groups])
static PyObject *pysso_verify_ticket(PyObject *self, PyObject *args,
                                     PyObject *kwargs) {
  const char *pub_key = NULL;
  Py_ssize_t pub_key_size = 0;
  const char *serialized_ticket = NULL;
  const char *service = NULL;
  const char *domain = NULL;
  const char *nonce = NULL;
  PyObject *groups_obj = NULL;
  const char **groups = NULL;
  static char *kwlist[] = {"public_key", "ticket", "service", "domain",
                           "nonce",      "groups", NULL};

  PyObject *result_obj = NULL;
  sso_ticket_t ticket = NULL;
  int err;

  if (!PyArg_ParseTupleAndKeywords(args, kwargs,
#if IS_PY3
                                   "y#sss|zO:verify_ticket",
#else
                                   "s#sss|zO:verify_ticket",
#endif
                                   kwlist, &pub_key, &pub_key_size,
                                   &serialized_ticket, &service, &domain,
                                   &nonce, &groups_obj)) {
    return NULL;
  }

  if (pub_key_size != SSO_PUBLIC_KEY_SIZE) {
    PyErr_SetString(PyExc_ValueError, "Bad public key size");
    goto fail;
  }

  if (groups_obj && groups_obj != Py_None) {
    groups = parse_groups(groups_obj);
  }

  err = sso_ticket_open(&ticket, serialized_ticket,
                        (const unsigned char *)pub_key);
  if (err != SSO_OK) {
    pysso_set_error(err);
    goto fail;
  }

  err = sso_validate(ticket, service, domain, nonce, groups);
  if (err != SSO_OK) {
    pysso_set_error(err);
    goto fail;
  }

  // Build a dictionary with the ticket values.
  result_obj = PyDict_New();
  PyDict_SetItem(result_obj, PyStr_FromString("user"),
                 PyStr_FromString(ticket->user));
  PyDict_SetItem(result_obj, PyStr_FromString("expires"),
                 PyLong_FromLong(ticket->expires));
  PyDict_SetItem(result_obj, PyStr_FromString("groups"),
                 groups_to_list(ticket->groups));

fail:
  if (groups) {
    free(groups);
  }
  if (ticket) {
    sso_ticket_free(ticket);
  }
  return result_obj;
}

// generate_keys()
static PyObject *pysso_generate_keys(PyObject *self, PyObject *args) {
  unsigned char pubkey[SSO_PUBLIC_KEY_SIZE];
  unsigned char privkey[SSO_SECRET_KEY_SIZE];
  int err;
  PyObject *pub, *priv;

  err = sso_generate_keys(pubkey, privkey);
  if (err != SSO_OK) {
    pysso_set_error(err);
    return NULL;
  }

  pub = PyBytes_FromStringAndSize((const char *)pubkey, SSO_PUBLIC_KEY_SIZE);
  priv = PyBytes_FromStringAndSize((const char *)privkey, SSO_SECRET_KEY_SIZE);
  return Py_BuildValue("OO", pub, priv);
}

static PyMethodDef pysso_methods[] = {
    /* The cast of the function is necessary since PyCFunction values
     * only take two PyObject* parameters, and keywdarg_parrot() takes
     * three.
     */
    {"create_and_sign_ticket", (PyCFunction)pysso_create_and_sign_ticket,
     METH_VARARGS | METH_KEYWORDS, "Sign a new SSO ticket."},
    {"verify_ticket", (PyCFunction)pysso_verify_ticket,
     METH_VARARGS | METH_KEYWORDS, "Verify a SSO ticket."},
    {"generate_keys", (PyCFunction)pysso_generate_keys, METH_VARARGS,
     "Generate a new SSO keypair."},
    {NULL, NULL, 0, NULL}};

static struct PyModuleDef pysso_module = {PyModuleDef_HEAD_INIT, "_pysso", NULL,
                                          -1, pysso_methods};

MODULE_INIT_FUNC(_pysso) {
  PyObject *m;

  m = PyModule_Create(&pysso_module);
  if (!m) {
    return NULL;
  }

  pysso_error = PyErr_NewException("_pysso.error", NULL, NULL);
  Py_INCREF(pysso_error);
  PyModule_AddObject(m, "error", pysso_error);
  return m;
}
