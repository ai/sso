import cgi
import logging
import sso
import urllib
from Cookie import SimpleCookie

log = logging.getLogger('sso_middleware')


def get_full_url(environ, base_url):
    if base_url:
        url = base_url
    else:
        url = environ['wsgi.url_scheme'] + '://'
        if environ.get('HTTP_HOST'):
            url += environ['HTTP_HOST']
        else:
            url += environ['SERVER_NAME']
            if environ['wsgi.url_scheme'] == 'https':
                if environ['SERVER_PORT'] != '443':
                    url += ':' + environ['SERVER_PORT']
            else:
                if environ['SERVER_PORT'] != '80':
                    url += ':' + environ['SERVER_PORT']
    url += environ.get('SCRIPT_NAME', '')
    url += environ.get('PATH_INFO', '')
    if environ.get('QUERY_STRING'):
        url += '?' + environ['QUERY_STRING']
    return url


def set_cookie(name, value):
    cookie = SimpleCookie()
    cookie[name] = value
    cookie[name]['path'] = "/"
    cookie[name]['secure'] = True
    return cookie.items()[0][1].OutputString()


def del_cookie(name):
    cookie = SimpleCookie()
    cookie[name] = value
    cookie[name]['path'] = '/'
    cookie[name]['secure'] = True
    cookie[name]['expires'] = -1
    return cookie.items()[0][1].OutputString()


def redirect(location, headers, environ, start_response):
    headers.append(('Location', location))
    start_response('302 Found', headers)
    return ['Found']


class SSOMiddleware:
    """SSO WSGI middleware.

    On successful authentication, it will modify the WSGI environment
    setting 'REMOTE_USER' to the name of the authenticated user, and
    'sso.ok' to True.
    """

    def __init__(self, next_app, service, domain, login_server, public_key,
                 cookie_name=None, allow_groups=None, base_url=None):
        self.next_app = next_app
        self.service = service
        self.base_url = base_url
        if not cookie_name:
            cookie_name = 'SSO_%s' % service.replace('.', '_').replace('/', '_').rstrip('_')
        self.cookie_name = cookie_name
        self.login_server = login_server
        if allow_groups is None:
            allow_groups = set()
        elif not isinstance(allow_groups, set):
            allow_groups = set(allow_groups)
        self.allow_groups = allow_groups
        self.verifier = sso.Verifier(
            public_key, service, domain, allow_groups)

    def __call__(self, environ, start_response):
        uri = environ['SCRIPT_NAME'] + environ['PATH_INFO']
        if uri == '/sso_login':
            query = cgi.parse(environ=environ, keep_blank_values=True)
	    ticket = query['t'][0]
            hdrs = [('Set-Cookie', set_cookie(self.cookie_name, ticket))]
            return redirect(query['d'][0], hdrs, environ, start_response)
        elif uri == '/sso_logout':
            hdrs = [('Set-Cookie', del_cookie(self.cookie_name)),
                    ('Content-Type', 'text/html')]
            start_response('200 OK', hdrs)
            return ['OK']
        else:
            cookies = SimpleCookie()
            cookies.load(environ.get('HTTP_COOKIE', ''))
            if self.cookie_name in cookies:
                ticket = cookies[self.cookie_name].value
                try:
                    tkt = self.verifier.verify(ticket)
                    environ['sso.ok'] = True
                    environ['REMOTE_USER'] = tkt.user()
                    return self.next_app(environ, start_response)
                except (TypeError, sso.Error) as e:
                    log.error('SSO authentication failed for %s: %s', uri, e)

            full_url = get_full_url(environ, self.base_url)
            redir_attrs = {'s': self.service, 'd': full_url}
            if self.allow_groups:
                redir_attrs['g'] = ','.join(self.allow_groups)
            redir_url = 'https://%s/?%s' % (self.login_server, urllib.urlencode(redir_attrs))
            return redirect(redir_url, [], environ, start_response)

