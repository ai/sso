import time
from ._pysso import generate_keys, create_and_sign_ticket, verify_ticket, error

Error = error

# Ticket is just a simple container for ticket attributes.
class Ticket(object):

    def __init__(self, user, service, domain, nonce=None, groups=None, ttl=3600, expires=None):
        self._user = user
        self._service = service
        self._domain = domain
        self._nonce = nonce
        self._groups = groups
        # 'expires' has precedence over 'ttl'.
        if expires is None:
            expires = int(time.time()) + ttl
        self._expires = expires

    def __eq__(self, t2):
        return (self.user() == t2.user() and
                self.service() == t2.service() and
                self.domain() == t2.domain() and
                self.nonce() == t2.nonce())

    def empty(self):
        return (not self._user and not self._domain
                and not self._service)

    def user(self):
        return self._user

    def domain(self):
        return self._domain

    def service(self):
        return self._service

    def nonce(self):
        return self._nonce

    def groups(self):
        return self._groups

    def expires(self):
        return self._expires

    def ttl(self):
        return int(self._expires - time.time())


class Signer(object):

    def __init__(self, sk):
        self._sk = sk

    def sign(self, t):
        return create_and_sign_ticket(
            self._sk,
            t.user(),
            t.service(),
            t.domain(),
            t.nonce(),
            t.groups(),
            t.ttl(),
        )


class Verifier(object):

    def __init__(self, pk, service, domain, ok_groups=None):
        self._pk = pk
        self._service = service
        self._domain = domain
        self._ok_groups = ok_groups

    def verify(self, encoded_tkt, nonce=None):
        result = verify_ticket(self._pk, encoded_tkt, self._service, self._domain, nonce, self._ok_groups)
        return Ticket(
            result['user'],
            self._service,
            self._domain,
            nonce,
            result['groups'],
            result['expires'],
        )
