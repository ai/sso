import base64
import unittest
import sso


class SignerTest(unittest.TestCase):

    def setUp(self):
        self.public, self.secret = sso.generate_keys()
        self.s = sso.Signer(self.secret)

    def _signok(self, tkt):
        signed = self.s.sign(tkt)
        self.assertTrue(isinstance(signed, str))
        self.assertGreater(len(signed), 0)
        #self.assertTrue('user' in base64.urlsafe_b64decode(signed))

    def test_sign_ticket(self):
        self._signok(sso.Ticket('user', 'service', 'domain'))

    def test_sign_ticket_with_ttl(self):
        self._signok(sso.Ticket('user', 'service', 'domain', ttl=3600))

    def test_sign_ticket_with_groups(self):
        groups = set(['group1', 'group2'])
        self._signok(sso.Ticket('user', 'service', 'domain', groups=groups))

    def test_sign_ticket_with_groups_and_ttl(self):
        groups = set(['group1', 'group2'])
        self._signok(sso.Ticket('user', 'service', 'domain', groups=groups, ttl=3600))

    def test_sign_ticket_with_nonce(self):
        self._signok(sso.Ticket('user', 'service', 'domain', nonce='testnonce'))

    # def test_sign_fail_with_empty_ticket(self):
    #     tkt = sso.Ticket()
    #     self.assertRaises(sso.Error, self.s.sign, tkt)

    def test_sign_fail_with_invalid_ticket(self):
        tkt = sso.Ticket('|', 'service', 'domain')
        self.assertRaises(sso.Error, self.s.sign, tkt)


if __name__ == '__main__':
    unittest.main()
