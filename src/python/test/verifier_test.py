from __future__ import print_function

import base64
import unittest
import sso


class VerifierTest(unittest.TestCase):

    def setUp(self):
        self.public, self.secret = sso.generate_keys()
        self.groups = set(['group1', 'group2'])
        self.signer = sso.Signer(self.secret)

    def test_create_verifier(self):
        v = sso.Verifier(self.public, 'service/', 'domain', self.groups)

    def _sign_token(self, tkt):
        return self.signer.sign(tkt)

    def test_verify_no_groups_ok(self):
        tkt = sso.Ticket('user', 'service/', 'domain')
        signed = self._sign_token(tkt)
        v = sso.Verifier(self.public, 'service/', 'domain')
        tkt2 = v.verify(signed)
        self.assertEquals(tkt, tkt2)

    def test_verify_ok(self):
        tkt = sso.Ticket('user', 'service/', 'domain', groups=set(['group1']))
        signed = self._sign_token(tkt)

        print('signed:', signed)

        v = sso.Verifier(self.public, 'service/', 'domain', self.groups)
        tkt2 = v.verify(signed)
        self.assertEquals(tkt, tkt2)

    def test_verify_ok_with_nonce(self):
        tkt = sso.Ticket('user', 'service/', 'domain', nonce='nonce')
        signed = self._sign_token(tkt)
        v = sso.Verifier(self.public, 'service/', 'domain')
        tkt2 = v.verify(signed, 'nonce')
        self.assertEquals(tkt, tkt2)
        self.assertEquals('user', tkt2.user())

    def test_verify_fail_with_bad_nonce(self):
        tkt = sso.Ticket('user', 'service/', 'domain', nonce='nonce')
        signed = self._sign_token(tkt)
        v = sso.Verifier(self.public, 'service/', 'domain')
        self.assertRaises(sso.Error, v.verify, signed, 'bad_nonce')

    def test_verify_fail_too_short(self):
        signed = base64.b64encode(b'some data').decode()
        v = sso.Verifier(self.public, 'service/', 'domain', self.groups)
        self.assertRaises(sso.Error, v.verify, signed)

    def test_verify_fail_not_signed(self):
        signed = base64.b64encode(b'some data' * 10).decode()
        v = sso.Verifier(self.public, 'service/', 'domain', self.groups)
        self.assertRaises(sso.Error, v.verify, signed)

    def test_verify_fail_expired(self):
        tkt = sso.Ticket('user', 'service/', 'domain', nonce='nonce', ttl=-1000)
        signed = self._sign_token(tkt)
        v = sso.Verifier(self.public, 'service/', 'domain')
        self.assertRaises(sso.Error, v.verify, signed, 'nonce')


class KnownDataVerifierTest(unittest.TestCase):

    def setUp(self):
        self.secret = b'\xd0\xd8\xe3v\x04\x91_v\x9f\x9e\xa3\xbb\xe2wd\x07\x89\xe4\xcc\x9a}\x11\xdd\xe8\xd6\xf2\xe3^\xa7\x03\xbe\x82\xc0\xda\xdb\xb4\x83v[\x05]O\x9f\xf5UM\x92\xb3\xedzC?\x15\xf4\xd8\xeb\xab\xbb\xd0rQ\x0b\xfe#'
        self.public = b'\xc0\xda\xdb\xb4\x83v[\x05]O\x9f\xf5UM\x92\xb3\xedzC?\x15\xf4\xd8\xeb\xab\xbb\xd0rQ\x0b\xfe#'

        self.signer = sso.Signer(self.secret)
        self.verifier = sso.Verifier(self.public, 'service/', 'sso.net')

    def test_sign_and_verify(self):
        tkt = sso.Ticket('user', 'service/', 'sso.net')
        signed = self.signer.sign(tkt)
        t2 = self.verifier.verify(signed)


if __name__ == '__main__':
    unittest.main()
