import os
import sys
import tempfile
import shutil
import unittest

import sso


class CommonTest(unittest.TestCase):

    def setUp(self):
        self.tmpdir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.tmpdir)

    def test_generate_keys(self):
        pub, sec = sso.generate_keys()
        self.assertEquals(32, len(pub))
        self.assertEquals(64, len(sec))

    #def test_read_key_from_file(self):
    #    pub, sec = sso.generate_keys()
    #    pkey_file = os.path.join(self.tmpdir, 'pubkey')
    #    with open(pkey_file, 'w') as fd:
    #        fd.write(sec)
    #    sec2 = sso.read_key_from_file(pkey_file)
    #    self.assertEquals(sec, sec2)


if __name__ == '__main__':
    unittest.main()
