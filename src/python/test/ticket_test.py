import unittest
import sso


class TicketTest(unittest.TestCase):

    def test_create_ticket(self):
        tkt = sso.Ticket('user', 'service', 'domain')
        self.assertTrue(tkt is not None)
        self.assertFalse(tkt.empty())
        self.assertEquals('user', tkt.user())
        self.assertEquals('service', tkt.service())
        self.assertEquals('domain', tkt.domain())
        expires = tkt.expires()
        self.assertTrue(isinstance(expires, int))
        self.assertTrue(expires > 0)

    # def test_create_empty_ticket(self):
    #     tkt = sso.Ticket()
    #     self.assertTrue(tkt.empty())

    def test_create_ticket_with_groups(self):
        groups = set(['g1', 'g2'])
        tkt = sso.Ticket('user', 'service', 'domain', groups=groups)
        self.assertFalse(tkt.empty())
        groups2 = tkt.groups()
        self.assertTrue(isinstance(groups2, set))
        self.assertEquals(groups, groups2)

    def test_create_ticket_with_nonce(self):
        tkt = sso.Ticket('user', 'service', 'domain', nonce='nonce')
        self.assertFalse(tkt.empty())
        self.assertEquals('nonce', tkt.nonce())


if __name__ == '__main__':
    unittest.main()
